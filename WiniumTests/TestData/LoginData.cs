﻿namespace WiniumTests.TestData
{
    public class LoginInputData
    {
        public string PhoneNumber { get; set; } = "";

        public string Pin { get; set; } = "";

        public string Cpr { get; set; } = "";

        public string CprPassword { get; set; } = "";

        public string Otp { get; set; } = "";

        public string BankName { get; set; } = "";

        public string BankAccount { get; set; } = "";

        public string Nickname { get; set; } = "";

        public string Email { get; set; } = "";

    }

    public class NewLoginInputData
    {
        public string PhoneNumber { get; set; } = "";

        public string Pin { get; set; } = "";

        public string Cpr { get; set; } = "";

        public string CprPassword { get; set; } = "";

        public string Otp { get; set; } = "";

        public string BankName { get; set; } = "";

        public string BankAccount { get; set; } = "";

        public string Nickname { get; set; } = "";

        public string Email { get; set; } = "";
    }

    public class LoginValidationData
    {

        public LoginValidationData()
        {
            PhoneNumber = "";
            Pin = "";
        }

        public LoginValidationData(LoginInputData inputData)
        {
            PhoneNumber = inputData.PhoneNumber;
            Pin = inputData.Pin;
        }


        public string PhoneNumber { get; set; }
        
        public string Pin { get; set; }
    }
}

﻿namespace WiniumTests.TestData
{
    public class TransactionInputData
    {
        public string RecipientPhoneNumber { get; set; }

        public string Amount { get; set; }

        public string Note { get; set; }

        public string TransactionId { get; set; }
    }

    public class MerchantData
    {
        public string PublicKey { get; set; }
        public string SecretKey { get; set; }

    }

    public class ConsumerApiData
    {
  
        public string ConsumerApiPin { get; set; }
        public string ConsumerApiOtp { get; set; }
        public string AppVersion { get; set; }
        public string ConsumerApiMsisdn { get; set; }
        public string ConsumerApiCountryCode { get; set; }
    }

    public class TransactionValidationData
    {
        public TransactionValidationData()
        {
        }

        public TransactionValidationData(TransactionInputData inputData)
        {
            NoteText = inputData.Note;
            MessageText = string.IsNullOrWhiteSpace(inputData.Note)
                ? "Swipp overførelse"
                : inputData.Note;
        }

        public string EnterAmountViewHeader { get; set; } = "";
        public string ChooseRecipientViewHeader { get; set; } = "";
        public string ConfirmationViewHeader { get; set; } = "";
        public string SummaryHeaderText { get; set; } = "";
        public string SummaryViewHeader { get; set; } = "";
        public string ErrorMessageViewHeader { get; set; } = "";
        public string DisplayAmount { get; set; } = "";

        public string MessageText { get; set; } = "";

        public string NoteText { get; set; } = "";

        public string RecipientName { get; set; } = "";
        public string RecipientPhoneNumber { get; set; } = "";

        public string TransactionId { get; set; } = "";
        public bool IsSmsButtonEnabled { get; set; }
    }
}

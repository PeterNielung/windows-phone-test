﻿
namespace WiniumTests.TestData
{
    public class SettingsUserInputData
    {
        public string TestNickname { get; set; } = "";
        public string NewMsisdn { get; set; } = "";
        public string NewMsisdnOtp { get; set; } = "";
        public string oldMsisdnOtp { get; set; } = "";
        public string TestPin { get; set; } = "";
        public string TestMail { get; set; } = "";
        public string BankAccount { get; set; } = "";
        public string NewBankAccount { get; set; } = "";

    }

    public class SettingsValidationData
    {
        public string Nickname { get; set; } = "";
        public string DisplayedNewMsisdn { get; set; } = "";
        public string DisplayedOldMsisdn { get; set; } = "";
        public string TestMsisdnOtp { get; set; } = "";
        public string Pin { get; set; } = "";
        public string Mail { get; set; } = "";
        public string SettingsViewHeader { get; set; } = "";
        public string InputPinViewHeader { get; set; } = ""; 
        public string ChangePinViewHeader { get; set; } = "";

    }

}

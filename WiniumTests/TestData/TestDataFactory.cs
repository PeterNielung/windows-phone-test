﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using OfficeOpenXml;

namespace WiniumTests.TestData
{
    public static class TestDataFactory
    {

        private enum KnownHeaderTypes
        {
            TestInfo, MerchantPublic, MerchantPrivate, PhoneNumber, Pin, RecipientPhoneNumber, Amount,
            Note, RecipientName, RecipientDisplayNumber, SummaryHeaderText, EnterAmountViewHeader,
            ChooseRecipientViewHeader, ConfirmationViewHeader, SummaryViewHeader, ErrorMessageViewHeader, IsSmsButtonEnabled,
            NewMsisdn, NewMsisdnOtp, OldMsisdnOtp, FormattedNewMsisdn, FormattedOldMsisdn, SettingsHeaderText, Nickname,
            NewNickname, Email, NewEmail, NewPin, InputPinViewHeader, ChangePinViewHeader,
            ConsumerApiPin, AppVersion, ConsumerApiMsisdn, ConsumerApiCountryCode, Cpr, CprPassword, BankName, BankAccount, Otp,
            ConsumerApiOtp, NewBankAccount, NewBankName, NewCprPassword, NewCpr, NewOtp, NewPhoneNumber
        }

        private static Dictionary<string, KnownHeaderTypes> _knownHeaders;
        private static Dictionary<string, KnownHeaderTypes> KnownHeaders {
            get
            {
                if (_knownHeaders == null)
                {
                    InitializeHeaderMap();
                }
                return _knownHeaders;
            }
        }

        private static void InitializeHeaderMap()
        {
            _knownHeaders = new Dictionary<string, KnownHeaderTypes>()
            {
                {"Test info", KnownHeaderTypes.TestInfo },
                {"MerchantPublic", KnownHeaderTypes.MerchantPublic },
                {"MerchantPrivate", KnownHeaderTypes.MerchantPrivate },
                {"PhoneNumber", KnownHeaderTypes.PhoneNumber },
                {"Pin", KnownHeaderTypes.Pin },
                {"RecipientPhoneNumber", KnownHeaderTypes.RecipientPhoneNumber },
                {"Amount", KnownHeaderTypes.Amount },
                {"Note", KnownHeaderTypes.Note },
                {"RecipientName", KnownHeaderTypes.RecipientName },
                {"RecipientDisplayNumber", KnownHeaderTypes.RecipientDisplayNumber },
                {"SummaryHeaderText", KnownHeaderTypes.SummaryHeaderText },
                {"EnterAmountViewHeader", KnownHeaderTypes.EnterAmountViewHeader },
                {"ChooseRecipientViewHeader", KnownHeaderTypes.ChooseRecipientViewHeader },
                {"ConfirmationViewHeader", KnownHeaderTypes.ConfirmationViewHeader },
                {"SummaryViewHeader", KnownHeaderTypes.SummaryViewHeader },
                {"ErrorMessageViewHeader", KnownHeaderTypes.ErrorMessageViewHeader },
                {"IsSmsButtonEnabled", KnownHeaderTypes.IsSmsButtonEnabled },
                {"NewMsisdn", KnownHeaderTypes.NewMsisdn },
                {"NewMsisdnOtp", KnownHeaderTypes.NewMsisdnOtp },
                {"OldMsisdnOtp", KnownHeaderTypes.OldMsisdnOtp },
                {"FormattedNewMsisdn", KnownHeaderTypes.FormattedNewMsisdn },
                {"FormattedOldMsisdn", KnownHeaderTypes.FormattedOldMsisdn },
                {"SettingsHeaderText", KnownHeaderTypes.SettingsHeaderText },
                {"NewNickname", KnownHeaderTypes.NewNickname },
                {"NewEmail", KnownHeaderTypes.NewEmail },
                {"NewPin", KnownHeaderTypes.NewPin },
                {"InputPinViewHeader", KnownHeaderTypes.InputPinViewHeader },
                {"ChangePinViewHeader", KnownHeaderTypes.ChangePinViewHeader },
                {"ConsumerApiPin", KnownHeaderTypes.ConsumerApiPin },
                {"AppVersion", KnownHeaderTypes.AppVersion },
                {"ConsumerApiMsisdn", KnownHeaderTypes.ConsumerApiMsisdn },
                {"ConsumerApiCountryCode", KnownHeaderTypes.ConsumerApiCountryCode },
                {"Cpr", KnownHeaderTypes.Cpr },
                {"CprPassword", KnownHeaderTypes.CprPassword },
                {"BankName", KnownHeaderTypes.BankName },
                {"BankAccount", KnownHeaderTypes.BankAccount },
                {"NewBankAccount", KnownHeaderTypes.NewBankAccount },
                {"Otp", KnownHeaderTypes.Otp },
                {"NewBankName", KnownHeaderTypes.NewBankName},
                {"NewCprPassword", KnownHeaderTypes.NewCprPassword},
                {"NewCpr", KnownHeaderTypes.NewCpr},
                {"NewOtp", KnownHeaderTypes.NewOtp},
                {"NewPhoneNumber", KnownHeaderTypes.NewPhoneNumber},
                {"Email", KnownHeaderTypes.Email},
                {"Nickname", KnownHeaderTypes.Nickname},
                {"ConsumerApiOtp", KnownHeaderTypes.ConsumerApiOtp}


            };
        }

        /// <summary>
        /// Creates TestDataSets from the specified Excel document. 
        /// You can specify how many sets you want in the parameter. 0 = all.
        /// </summary>
        /// <param name="numberOfSets"></param>
        /// <returns>List with TestDataSets</returns>
        public static List<TestDataSet> GetTestDataSets(int numberOfSets, string excelDocumentName = "UITestData")
        {
            var importedData = ImportTestData(excelDocumentName);
            var dataSets = new List<TestDataSet>();
            if (numberOfSets == 0)
            {
                dataSets = importedData.ToList();
            }
            else
            {
                if (importedData.Count < numberOfSets)
                {
                    throw new ArgumentOutOfRangeException($"importeData does not contain that amount of data. ImportedData contains: {importedData.Count} datasets");
                }

                dataSets = importedData.Take(numberOfSets).ToList();
            }

            return dataSets;
        }

        
    
        private static List<TestDataSet> ImportTestData(string excelDocumentName)
        {
            var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            var fileToRead = new FileInfo($"{baseDirectory}\\TestData\\{excelDocumentName}.xlsx");
            
            //load worksheet from file. Assuming we always need worksheet #1. 
            using (ExcelPackage xlPackage = new ExcelPackage(fileToRead))
            {
                ExcelWorksheet xl = xlPackage.Workbook.Worksheets[1]; //worksheets are indexed starting with 1

                //Dataplacement in document
                var startingRow = 1;

                //Creates a list container
                var listOfDataSets = new List<TestDataSet>();
                
                //Keep reading rows until we encounter EOF message
                bool continueParsing = true;
                for (int currentRow = startingRow; continueParsing; currentRow++)
                {
                    //Read the parsermessage on this row:
                    var parserMessage = GetCellValue(currentRow, 1, xl);
                    
                    if (parserMessage.Equals("EOF"))
                    {
                        //No more testdata
                        continueParsing = false;
                    }
                    if (parserMessage.Equals("Global"))
                    {
                        var flagName = GetCellValue(currentRow, 2, xl);
                        
                        if (flagName.Equals("NoTestDataMeansInconclusive"))
                        {
                            TestDataSet.TestsWithoutDataShouldBeInconclusive =
                                bool.Parse(GetCellValue(currentRow, 3, xl));
                        }
                        if (flagName.Equals("LongAnswerTimeHeaderText"))
                        {
                            TestDataSet.WaitingForApiHeader = GetCellValue(currentRow, 3, xl);
                        }
                    }
                    if (parserMessage.Equals("Test"))
                    {
                        //A row with testdata was found
                        var dS = new TestDataSet
                        {
                            LoginInputData = new LoginInputData(),
                            TransactionInputData = new TransactionInputData(),
                            MerchantData = new MerchantData(),
                            TransactionValidationData = new TransactionValidationData(),
                            SettingsUserInputData = new SettingsUserInputData(),
                            LoginValidationData = new LoginValidationData(),
                            SettingsValidationData = new SettingsValidationData(),
                            ConsumerApiData =  new ConsumerApiData(),
                            NewLoginInputData =  new NewLoginInputData()
                        };

                        //Parserinformation: Amount of columns and header/explanation row number:
                        var headerRowNo = Int32.Parse(GetCellValue(currentRow, 2, xl));
                        var amountOfCols = Int32.Parse(GetCellValue(currentRow, 3, xl));

                        for (int currentCol = 4; currentCol < amountOfCols + 4; currentCol++)
                        {
                            //Read data of current cell:
                            var data = GetCellValue(currentRow, currentCol, xl);

                            //If there is data, what data is it?
                            if (!String.IsNullOrEmpty(data))
                            {
                                var headerText = GetCellValue(headerRowNo, currentCol, xl);
                                var headerType = KnownHeaders[headerText];

                                switch (headerType)
                                {
                                    case KnownHeaderTypes.TestInfo:
                                        dS.TestDataId = data;
                                        break;
                                    case KnownHeaderTypes.PhoneNumber:
                                        dS.LoginInputData.PhoneNumber = data;
                                        break;
                                    case KnownHeaderTypes.Pin:
                                        dS.LoginInputData.Pin = data;
                                        dS.LoginValidationData.Pin = data;
                                        break;
                                    case KnownHeaderTypes.RecipientPhoneNumber:
                                        dS.TransactionInputData.RecipientPhoneNumber = data;
                                        break;
                                    case KnownHeaderTypes.Amount:
                                        var amount = data;
                                        dS.TransactionInputData.Amount = data;

                                        //Format the amount to the way it is displayed on the phonescreen
                                        decimal displayAmount;
                                        Decimal.TryParse(amount, NumberStyles.AllowDecimalPoint, new NumberFormatInfo(),
                                            out displayAmount);
                                        dS.TransactionInputData.Amount = amount.Contains(".")
                                            ? amount.Replace(".", ",")
                                            : amount;
                                        var preformatAmount = displayAmount.ToString("N2");
                                        dS.TransactionValidationData.DisplayAmount = preformatAmount.EndsWith(",00") ? preformatAmount.TrimEnd('0').TrimEnd(',') : preformatAmount;
                                        break;
                                    case KnownHeaderTypes.Note:
                                        dS.TransactionInputData.Note = data;
                                        dS.TransactionValidationData.NoteText = data;
                                        dS.TransactionValidationData.MessageText = string.IsNullOrWhiteSpace(data) ? "Swipp overførelse" : data;
                                        break;
                                    case KnownHeaderTypes.SummaryHeaderText:
                                        dS.TransactionValidationData.SummaryHeaderText = data;
                                        break;
                                    case KnownHeaderTypes.EnterAmountViewHeader:
                                        dS.TransactionValidationData.EnterAmountViewHeader = data;
                                        break;
                                    case KnownHeaderTypes.ChooseRecipientViewHeader:
                                        dS.TransactionValidationData.ChooseRecipientViewHeader = data;
                                        break;
                                    case KnownHeaderTypes.ConfirmationViewHeader:
                                        dS.TransactionValidationData.ConfirmationViewHeader = data;
                                        break;
                                    case KnownHeaderTypes.SummaryViewHeader:
                                        dS.TransactionValidationData.SummaryViewHeader = data;
                                        break;
                                    case KnownHeaderTypes.ErrorMessageViewHeader:
                                        dS.TransactionValidationData.ErrorMessageViewHeader = data;
                                        break;
                                    case KnownHeaderTypes.IsSmsButtonEnabled:
                                        bool isSmsButtonEnabled = false;
                                        bool.TryParse(data, out isSmsButtonEnabled);
                                        dS.TransactionValidationData.IsSmsButtonEnabled = isSmsButtonEnabled;
                                        break;
                                    case KnownHeaderTypes.MerchantPublic:
                                        dS.MerchantData.PublicKey = data;
                                        break;
                                    case KnownHeaderTypes.MerchantPrivate:
                                        dS.MerchantData.SecretKey = data;
                                        break;
                                    case KnownHeaderTypes.RecipientName:
                                        dS.TransactionValidationData.RecipientName = data;
                                        break;
                                    case KnownHeaderTypes.RecipientDisplayNumber:
                                        dS.TransactionValidationData.RecipientPhoneNumber = data;
                                        break;
                                    case KnownHeaderTypes.NewMsisdn:
                                        dS.SettingsUserInputData.NewMsisdn = data;
                                        break;
                                    case KnownHeaderTypes.NewMsisdnOtp:
                                        dS.SettingsUserInputData.NewMsisdnOtp = data;
                                        break;
                                    case KnownHeaderTypes.OldMsisdnOtp:
                                        dS.SettingsUserInputData.oldMsisdnOtp = data;
                                        break;
                                    case KnownHeaderTypes.FormattedNewMsisdn:
                                        dS.SettingsValidationData.DisplayedNewMsisdn = data;
                                        break;
                                    case KnownHeaderTypes.FormattedOldMsisdn:
                                        dS.SettingsValidationData.DisplayedOldMsisdn = data;
                                        break;
                                    case KnownHeaderTypes.SettingsHeaderText:
                                        dS.SettingsValidationData.SettingsViewHeader = data;
                                        break;
                                    case KnownHeaderTypes.NewEmail:
                                        dS.NewLoginInputData.Email = data;
                                        dS.SettingsUserInputData.TestMail = data;
                                        break;
                                    case KnownHeaderTypes.NewNickname:
                                        dS.NewLoginInputData.Nickname = data;
                                        dS.SettingsUserInputData.TestNickname = data;
                                        break;
                                    case KnownHeaderTypes.NewPin:
                                        dS.NewLoginInputData.Pin = data;
                                        dS.SettingsUserInputData.TestPin = data;
                                        break;
                                    case KnownHeaderTypes.InputPinViewHeader:
                                        dS.SettingsValidationData.InputPinViewHeader = data;
                                        break;
                                    case KnownHeaderTypes.ChangePinViewHeader:
                                        dS.SettingsValidationData.ChangePinViewHeader = data;
                                        break;
                                    case KnownHeaderTypes.ConsumerApiOtp:
                                        dS.ConsumerApiData.ConsumerApiOtp = data;
                                        break;
                                    case KnownHeaderTypes.ConsumerApiPin:
                                        dS.ConsumerApiData.ConsumerApiPin = data;
                                        break;
                                    case KnownHeaderTypes.AppVersion:
                                        dS.ConsumerApiData.AppVersion = data;
                                        break;
                                    case KnownHeaderTypes.ConsumerApiMsisdn:
                                        dS.ConsumerApiData.ConsumerApiMsisdn = data;
                                        break;
                                    case KnownHeaderTypes.ConsumerApiCountryCode:
                                        dS.ConsumerApiData.ConsumerApiCountryCode = data;
                                        break;
                                    case KnownHeaderTypes.Cpr:
                                        dS.LoginInputData.Cpr = data;
                                        break;
                                    case KnownHeaderTypes.CprPassword:
                                        dS.LoginInputData.CprPassword = data;
                                        break;
                                    case KnownHeaderTypes.BankName:
                                        dS.LoginInputData.BankName = data;
                                        break;
                                    case KnownHeaderTypes.BankAccount:
                                        dS.LoginInputData.BankAccount = data;
                                        dS.SettingsUserInputData.BankAccount = data;
                                        break;
                                    case KnownHeaderTypes.NewBankAccount:
                                        dS.SettingsUserInputData.NewBankAccount = data;
                                        dS.NewLoginInputData.BankAccount = data;
                                        break;
                                    case KnownHeaderTypes.Email:
                                        dS.LoginInputData.Email = data;
                                        break;
                                    case KnownHeaderTypes.Otp:
                                        dS.LoginInputData.Otp = data;
                                        break;
                                    case KnownHeaderTypes.Nickname:
                                        dS.LoginInputData.Nickname = data;
                                        break;
                                    case KnownHeaderTypes.NewPhoneNumber:
                                        dS.NewLoginInputData.PhoneNumber = data;
                                        break;
                                    case KnownHeaderTypes.NewOtp:
                                        dS.NewLoginInputData.Otp = data;
                                        break;
                                    case KnownHeaderTypes.NewCpr:
                                        dS.NewLoginInputData.Cpr = data;
                                        break;
                                    case KnownHeaderTypes.NewCprPassword:
                                        dS.NewLoginInputData.CprPassword = data;
                                        break;
                                    case KnownHeaderTypes.NewBankName:
                                        dS.NewLoginInputData.BankName = data;
                                        break;

                                    default:
                                        throw new ArgumentOutOfRangeException(headerType.ToString());
                                }
                            }
                        }
                        listOfDataSets.Add(dS);
                    }
                }

                return listOfDataSets;
            }
        }

        private static string GetCellValue(int row, int col, ExcelWorksheet ws)
        {
            return ws.Cell(row, col).Value;
        }
    }
}

﻿namespace WiniumTests.TestData
{
    public class SetupData
    {
        private const string VersionNumber = "1.1.0.4";

        private const string PackageLocation =
            "C:\\GitRepo\\swipp-consumer-app\\Swipp.Consumer\\Swipp.Consumer.WindowsPhone\\AppPackages\\Test\\";

        public string UsedAppxFile = $"{PackageLocation}Swipp.Consumer.WindowsPhone_{VersionNumber}_x86_Debug_Test\\Swipp.Consumer.WindowsPhone_{VersionNumber}_x86_Debug.appx";
        public string Win81Device = "Emulator 8.1 WVGA 4 inch 512MB";
        public string Win10Device = "Mobile Emulator 10.0.10586.0 WVGA 4 inch 512MB";
    }
}

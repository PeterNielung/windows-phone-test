﻿namespace WiniumTests.TestData
{
    public class TestDataSet
    {
        public string TestDataId { get; set; } = "";
        public static bool TestsWithoutDataShouldBeInconclusive { get; set; } = false;
        public static string WaitingForApiHeader { get; set; } = "";
        public LoginInputData LoginInputData { get; set; }
        public NewLoginInputData NewLoginInputData { get; set; }
        public LoginValidationData LoginValidationData { get; set; }
        public TransactionInputData TransactionInputData { get; set; }
        public TransactionValidationData TransactionValidationData { get; set; }
        public MerchantData MerchantData { get; set; }
        public ConsumerApiData ConsumerApiData { get; set; }
        public SettingsUserInputData SettingsUserInputData { get; set; }
        public SettingsValidationData SettingsValidationData { get; set; }
    }
}

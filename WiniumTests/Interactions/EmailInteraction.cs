﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class EmailInteraction : BaseInteraction
    {
        public EmailInteraction(UiElementSearch map) : base(map)
        {
        }

        public void InputEmailAndSave(string newEmail)
        {
            var emailTextBox =
                Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.EmailViewEmailInputTextBox);

            emailTextBox.Clear();
            emailTextBox.SendKeys(newEmail);
            emailTextBox.Submit();
        }

        public void ToggleMarketing()
        {
            var marketingSwitch =
                Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.EmailViewMarketingToggleSwitch);
            marketingSwitch.Click();
        }

        public void Save()
        {
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.EmailViewSaveChangesButton).Click();
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class SettingsInteraction : BaseInteraction
    {
        public SettingsInteraction(UiElementSearch map) : base(map)
        {
        }

        public void Validate(string expectedHeader)
        {
           Map.WaitForHeader(expectedHeader);
        }

        public NicknameInteraction StartNicknameInteraction()
        {
            var nicknameSettingsButton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewNicknameButton);
            nicknameSettingsButton.Click();

            return new NicknameInteraction(Map);
        }

        public PhoneNumberInteraction StartPhoneNumberInteraction()
        {
            var msisdnSettingsButton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewMsisdnButton);
            msisdnSettingsButton.Click();

            return new PhoneNumberInteraction(Map);
        }

        public PinInteraction StartPinInteraction()
        {
            var pinSettingsButton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewPinButton);
            pinSettingsButton.Click();

            return new PinInteraction(Map);
        }

        public EmailInteraction StartEmailInteraction()
        {
            var emailSettingsButton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewEmailButton);
            emailSettingsButton.Click();

            return new EmailInteraction(Map);
        }

        public DailyLimitInteraction StartDailyLimitInteraction()
        {
            var dailyLimitSettingsButton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewAmountLimitButton);
            dailyLimitSettingsButton.Click();

            return new DailyLimitInteraction(Map);
        }

        public void ChangeAccount(string accountNumber)
        {
            //Navigate to Change Account.
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewBankAccountButton).Click();

            //Click new accountNumber
            Map.FindUiElementByIdForSpecifiedTime(accountNumber).Click();
        }

        public void PressChangeAccountButton()
        {
            //Navigate to Change Account.
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewBankAccountButton).Click();
            Wait();
        }

        public void PressChangeBankButton()
        {
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewBankButton).Click();
            Wait();
        }

        public void ChangeBank(string pin)
        {
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewBankButton).Click();
            Wait();
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MessageViewConfirmButton).Click();
            Wait();
            var pinInteraction = new PinInteraction(Map);
            pinInteraction.InputPin(pin);
            pinInteraction.PressNext();
            Wait(5000);
            Assert.IsTrue(Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.StartButton).Displayed);
        }

        public void DeleteAgreement(string pin)
        {
            Map.ScrollToDeleteButton();

            Wait();

            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewDeleteAgreementButton).Click();
            Wait();
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MessageViewConfirmButton).Click();
            Wait();
            var pinInteraction = new PinInteraction(Map);
            pinInteraction.InputPin(pin);
            pinInteraction.PressNext();
            Wait(5000);

            Assert.IsTrue(Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.StartButton).Displayed);
        }

        public void BlockAgreement(string pin)
        {
            Map.ScrollToDeleteButton();

            Wait();

            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewBlockAgreementButton).Click();
            Wait();
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MessageViewConfirmButton).Click();
            Wait();
            var pinInteraction = new PinInteraction(Map);
            pinInteraction.InputPin(pin);
            pinInteraction.PressNext();
            Wait(5000);
        }

        public void ValidateBlockView(string expectedTitleText, string expectedErrorText)
        {
            var actualTitleText =
                Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.BackBaseHeaderViewText).Text;
            var actualErrorText =
                Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.ErrorMessageViewMessageText).Text;
            Assert.AreEqual(expectedTitleText, actualTitleText);
            Assert.AreEqual(expectedErrorText, actualErrorText);
        }

        public string CurrentBankName
        {
            get { 
                Wait(2000);
                return Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewBankButtonValue).Text;
            }
        }

        public string CurrentNickname
        {
            get
            {
                //Allow UI to update with new data... This can happen after the view has been loaded.
                Wait(2000);
                return Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewNicknameButtonValue).Text;
            }
        }

        public string CurrentPhonenumber
        {
            get
            {
                //Allow UI to update with new data... This can happen after the view has been loaded.
                Wait(2000);
                return Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewMsisdnButtonValue).Text;
            }
        }

        public void AssertEmailMatches(string emailThatShouldMatch)
        {
            for (var tries = 0; tries < 3; tries++)
            {
                try
                {
                    Assert.AreEqual(emailThatShouldMatch, CurrentEmail);
                    return;
                }
                catch (Exception) {
                }
                
                //No need for this, as each property in settings waits 2000ms before returning
                //Wait(500);
            }
            throw new AssertFailedException($"Email did not match with: {emailThatShouldMatch}, or UI updated too slow");
        }

        public void AssertPhonenumberMatches(string phonenumberThatShouldMatch)
        {
            for (var tries = 0; tries < 3; tries++)
            {
                try
                {
                    Assert.AreEqual(phonenumberThatShouldMatch, CurrentPhonenumber);
                    return;
                }
                catch (Exception)
                {
                }

                //No need for this, as each property in settings waits 2000ms before returning
                //Wait(500);
            }
            throw new AssertFailedException($"Phonenumber did not match with: {phonenumberThatShouldMatch}, or UI updated too slow");
        }

        public void AssertCurrentNicknameMatches(string nicknameThatShouldMatch)
        {
            for (var tries = 0; tries < 3; tries++)
            {
                try
                {
                    Assert.AreEqual(nicknameThatShouldMatch, CurrentNickname);
                    return;
                }
                catch (Exception)
                {
                }

                //No need for this, as each property in settings waits 2000ms before returning
                //Wait(500);
            }
            throw new AssertFailedException($"Nickname did not match with: {nicknameThatShouldMatch}, or UI updated too slow");
        }

        public void AssertCurrenAccountMatches(string accountThatShouldMatch)
        {
            for (var tries = 0; tries < 3; tries++)
            {
                try
                {
                    Assert.AreEqual(accountThatShouldMatch, CurrentAccount);
                    return;
                }
                catch (Exception)
                {
                }

                //No need for this, as each property in settings waits 2000ms before returning
                //Wait(500);
            }
            throw new AssertFailedException($"Account did not match with: {accountThatShouldMatch}, or UI updated too slow");
        }


        public string CurrentEmail
        {
            get
            {
                //Allow UI to update with new data... This can happen after the view has been loaded.
                Wait(2000);
                return Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewEmailButtonValue).Text;
            }
        }

        public string CurrentDailyLimit {
            get
            {
                //Allow UI to update with new data... This can happen after the view has been loaded.
                Wait(2000);
                return Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewAmountLimitButtonValue).Text;
            }
        }

        public string CurrentAccount
        {
            get
            {
                Wait(2000);
                return Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SettingsViewBankAccountButtonValue).Text;
            }
        }
    }
}

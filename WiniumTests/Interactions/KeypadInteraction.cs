﻿using System;
using OpenQA.Selenium;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class KeypadInteraction : BaseInteraction
    {

        public KeypadInteraction(UiElementSearch map) : base(map)
        {
        }

        public void Next()
        {
            var button = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypadNext);
            button.Click();
        }

        public void Delete()
        {
            var button = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypadDeleteComma);
            button.Click();
        }

        public void InputNumberToKeypad(string number)
        {
            //Wait a while to make sure that keypad + whatever needs it has loaded properly
            Wait();

            for (var digit = 0; digit < number.Length; digit++)
            {
                IWebElement buttonToClick;
                switch (number[digit])
                {
                    case '0':
                        buttonToClick = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypad0);
                        break;
                    case '1':
                        buttonToClick = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypad1);
                        break;
                    case '2':
                        buttonToClick = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypad2);
                        break;
                    case '3':
                        buttonToClick = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypad3);
                        break;
                    case '4':
                        buttonToClick = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypad4);
                        break;
                    case '5':
                        buttonToClick = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypad5);
                        break;
                    case '6':
                        buttonToClick = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypad6);
                        break;
                    case '7':
                        buttonToClick = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypad7);
                        break;
                    case '8':
                        buttonToClick = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypad8);
                        break;
                    case '9':
                        buttonToClick = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypad9);
                        break;
                    case ',':
                    case '.':
                        buttonToClick = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypadDeleteComma);
                        break;
                    default:
                        throw new NotSupportedException("Parsed something that was not recognized on the keypad: " + digit.ToString());
                }
                Wait(400);
                buttonToClick.Click();
            }

            //Maybe check if we missed a key and are at the pinview/otpview and then retry?

        }

        public bool IsKeypadAccessible
        {
            get
            {
                //If a keypadbutton is visible, we assume that the keypad is visible. 
                //NOTE: The keypadnext button is not always present, so we cannot use that button to test this.
                return Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypad1).Displayed;
            }
        }
    }
}

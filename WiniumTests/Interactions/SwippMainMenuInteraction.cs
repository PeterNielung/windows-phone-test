﻿using System;
using OpenQA.Selenium;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class SwippMainMenuInteraction : BaseInteraction
    {
        private readonly HistoryInteraction _historyScreen;
        private readonly SettingsInteraction _settingsScreen;
        

        public SwippMainMenuInteraction(UiElementSearch map) : base(map)
        {
            _historyScreen = new HistoryInteraction(map);
            _settingsScreen = new SettingsInteraction(map);
        }

        public bool IsMenuAccessible
        {
            get { return Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainMenuShowMenu).Displayed; }
        }

        /// <summary>
        /// Displays the main menu before any button can be tapped.
        /// </summary>
        public void ShowMenu()
        {
            MainMenuButton(MainMenuButtons.ShowMenu);
        }

        public void CloseMenu()
        {
            MainMenuButton(MainMenuButtons.CloseMenu);
        }

        public void Swipp()
        {
            MainMenuButton(MainMenuButtons.Swipp);
        }

        public void History()
        {
            MainMenuButton(MainMenuButtons.History);
        }

        public void ValidateHistoryView(string expectedHeader)
        {
            _historyScreen.Validate(expectedHeader);
        }

        public void Settings()
        {
            MainMenuButton(MainMenuButtons.Settings);
        }

        public void ValidateSettingsView(string expectedHeader)
        {
            _settingsScreen.Validate(expectedHeader);
        }

        public void Terms()
        {
            MainMenuButton(MainMenuButtons.Terms);
        }

        public void Help()
        {
            MainMenuButton(MainMenuButtons.Help);
        }

        public void Logout()
        {
            MainMenuButton(MainMenuButtons.Logout);
        }

        private void MainMenuButton(MainMenuButtons button)
        {
            IWebElement clickedbutton;
            switch (button)
            {
                case MainMenuButtons.ShowMenu:
                    clickedbutton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainMenuShowMenu);
                    break;
                case MainMenuButtons.CloseMenu:
                    clickedbutton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainMenuCloseMenu);
                    break;
                case MainMenuButtons.Swipp:
                    clickedbutton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainMenuSwipp);
                    break;
                case MainMenuButtons.History:
                    clickedbutton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainMenuHistory);
                    break;
                case MainMenuButtons.Settings:
                    clickedbutton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainMenuSettings);
                    break;
                case MainMenuButtons.Terms:
                    clickedbutton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainMenuTerms);
                    break;
                case MainMenuButtons.Help:
                    clickedbutton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainMenuHelp);
                    break;
                case MainMenuButtons.Logout:
                    clickedbutton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainMenuLogout);
                    break;
                default:
                    throw new NotSupportedException("Mainmenu did not support button: " + button.ToString());
            }

            clickedbutton.Click();
            Wait();
        }

        private enum MainMenuButtons
        {
            ShowMenu,
            CloseMenu,
            Swipp,
            History,
            Settings,
            Terms,
            Help,
            Logout
        }
    }
}

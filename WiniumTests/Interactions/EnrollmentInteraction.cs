﻿using System.Runtime.Remoting.Messaging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class EnrollmentInteraction : BaseInteraction
    {
        private readonly KeypadInteraction _keypadInteraction;
        private readonly NemidViewInteraction _nemidViewInteraction;

        public EnrollmentInteraction(UiElementSearch map) : base(map)
        {
            _keypadInteraction = new KeypadInteraction(map);
            _nemidViewInteraction = new NemidViewInteraction(map);
        }

        public NemidViewInteraction NemidScreen => _nemidViewInteraction;

        public void BlockedViewPressNextButton()
        {
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SendSMSButton).Click();
        }

        public void PressIntroStartButton()
        {
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.StartButton).Click();
            Wait();
        }

        public void EnterMsisdn(string msisdn)
        {
            _keypadInteraction.InputNumberToKeypad(msisdn);
        }

        public void EnterOtp(string otp)
        {
            _keypadInteraction.InputNumberToKeypad(otp);
        }

        public void PressKeypadNextButton()
        {
            Wait();
            _keypadInteraction.Next();
            Wait();
        }

        public void Login(string pin)
        {
            _keypadInteraction.InputNumberToKeypad(pin);
        }

        public void EnterCpr(string cpr)
        {
            _keypadInteraction.InputNumberToKeypad(cpr);
        }

        public void EnterBankName(string bankName)
        {
            var searchBox = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.BankViewSearchBox);
            searchBox.SendKeys(bankName);
        }

        public void ChooseBank(string bankName)
        {
            Map.FindUiElementByIdForSpecifiedTime(bankName).Click();
        }

        public void ChooseAccount(string accountNumber)
        {
            Map.FindUiElementByIdForSpecifiedTime(accountNumber).Click();
        }

        public void EnterNickname(string nickname)
        {
            var nickNameInput = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NicknameViewNicknameTextBox);
            nickNameInput.Clear();
            nickNameInput.SendKeys(nickname);
        }

        public void ConfirmNickname()
        {
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.BackBaseHeaderViewText).Click();
            Wait();
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NicknameViewSaveButton).Click();
        }

        public void EnterEmail(string email)
        {
            var inputBox = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.EmailViewEmailInputTextBox);
            inputBox.SendKeys(email);
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.BackBaseHeaderViewText).Click();
        }

        public void ToggleMarketing()
        {
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.EmailViewMarketingToggleSwitch).Click();
        }

        public void ConfirmEmail()
        {
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.EmailViewSaveChangesButton).Click();
        }

        public void EnterPin(string pin)
        {
            _keypadInteraction.InputNumberToKeypad(pin);
            _keypadInteraction.Next();
        }
        
        public void ConfirmPin(string pin)
        { 
            EnterPin(pin);
        }

        public void WaitForHeader(string headerText)
        {
            Map.WaitForHeader(headerText);
        }

        public void ConfirmTerms()
        {
            
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.AcceptTermsCheckBox).Click();
            Wait(100);
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.AcceptTermsAcceptButton).Click();
            Wait(5000);
        }

        public void ValidateError(string expectedError, string expectedHeader)
        {
            var errorText =
                Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.ErrorMessageViewMessageText).Text;
            var textArray = errorText.Split('=');
            var actualError = textArray[2];
            var actualHeader = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.BackBaseHeaderViewText).Text;

            Assert.AreEqual(expectedHeader, actualHeader);
            Assert.AreEqual(expectedError, actualError);

        }
    }
}

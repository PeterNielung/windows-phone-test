﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class PhoneNumberInteraction : BaseInteraction
    {
        public PhoneNumberInteraction(UiElementSearch map) : base(map)
        {
        }

        public void InputPhoneNumber(string number)
        {
            var keypad = new KeypadInteraction(Map);

            //Clear old phonenumber.
            var currentPhonenumber =
                Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.PhoneNumberViewPhonenumber);
            while (currentPhonenumber.Text != "")
            {
                keypad.Delete();
            }

            //Input new phonenumber
            keypad.InputNumberToKeypad(number);
        }

        public void InputOtp(string otp)
        {
            var keypad = new KeypadInteraction(Map);
            
            //Input new phonenumber
            keypad.InputNumberToKeypad(otp);
        }

        public void Next()
        {
            var keypad = new KeypadInteraction(Map);
            keypad.Next();
        }
    }
}

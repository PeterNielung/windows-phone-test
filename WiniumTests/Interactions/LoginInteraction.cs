﻿using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class LoginInteraction : BaseInteraction
    {
        private readonly KeypadInteraction _keypadInteraction;
        public LoginInteraction(UiElementSearch map) : base(map)
        {
            _keypadInteraction = new KeypadInteraction(map);
        }

        public void Login(string pin)
        {
            _keypadInteraction.InputNumberToKeypad(pin);
            Wait();
        }
    }
}

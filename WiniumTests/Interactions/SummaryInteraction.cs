﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using WiniumTests.TestData;
using WiniumTests.Utility;
using Assert = NUnit.Framework.Assert;

namespace WiniumTests.Interactions
{
    public class SummaryInteraction : BaseInteraction
    {
        public SummaryInteraction(UiElementSearch map) : base(map) { }

        public void CloseCompletedFlow()
        {
            var summaryCloseButton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SummaryViewCloseButton);
            summaryCloseButton.Click();
            Wait();
        }

        public void CancelRequest()
        {
            var summaryCloseButton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SummaryViewCloseButton);
            summaryCloseButton.Click();
            Wait();
        }

        public void PressPopupYes()
        {
            Map.PopupboxPushAccept();
            Wait();
        }

        public void Validate(TransactionValidationData expectedData)
        {
            ValidateSummaryViewHeaderText(expectedData.SummaryViewHeader);
            ValidateSummaryHeaderText(expectedData.SummaryHeaderText);
            ValidateRecipient(expectedData.RecipientName, expectedData.RecipientPhoneNumber);
            ValidateAmount(expectedData.DisplayAmount);
            ValidateMessage(expectedData.MessageText);
        }

        public string GetTransactionId()
        {
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SummaryViewTransactionTimeSection).Click();
            var transactionIdText = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SummaryViewTransactionId).Text;
            var transactionId = transactionIdText.Split(null)[2];

            return transactionId;
        }

        //Validates the Views HeaderText example: Overførelse gennemført.
        private void ValidateSummaryViewHeaderText(string expectedViewHeaderText)
        {
            Map.WaitForHeader(expectedViewHeaderText);
        }

        //validates the header text for the summary example: Overført til.
        private void ValidateSummaryHeaderText(string expectedHeaderText)
        {
            var actualHeaderText = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SummaryViewHeaderText);
            Assert.AreEqual(expectedHeaderText, actualHeaderText.Text);
        }
        private void ValidateRecipient(string expectedName, string expectedPhoneNumber)
        {
            var actualName = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SummaryViewName);
            var actualPhoneNumber = "";

            try
            {
                actualPhoneNumber = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SummaryViewPhoneNumber, 100).Text;
            }
            catch(NotFoundException)
            {
                Debug.WriteLine("Exception thrown, Payed via Token thus the msisdn test does not exist");
                //This can happen for instance, if we pay via a token. Then we dont pay to a number, thus the msisdn text does not exist.
            }

            Assert.AreEqual(expectedName, actualName.Text);
            Assert.AreEqual(expectedPhoneNumber, actualPhoneNumber);
        }

        private void ValidateAmount(string expectedAmount)
        {
            var expectedDisplayedAmount = $"{expectedAmount} kr.";
            var actualDisplayedAmount = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SummaryViewAmount);
            Assert.AreEqual(expectedDisplayedAmount, actualDisplayedAmount.Text);
        }

        private void ValidateMessage(string expectedMessage)
        {
            var actualMessage = "";
            try
            {
                actualMessage = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.SummaryViewNote, 100).Text;
            }
            catch (NotFoundException)
            {
                //This can happen for instance, if we pay via a token. Then the note text does not exist.
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }
    }
}

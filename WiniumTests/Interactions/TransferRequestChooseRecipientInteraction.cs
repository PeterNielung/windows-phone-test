﻿using NUnit.Framework;
using WiniumTests.TestData;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class TransferRequestChooseRecipientInteraction : BaseInteraction
    {

        public TransferRequestChooseRecipientInteraction(UiElementSearch map) : base(map)
        {
        }

        public void InputRecipient(string recipientSearchString)
        {
            var recipientSearchText =
                Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.TransferRequestChooseRecipientSearchTextBox);
            recipientSearchText.SendKeys(recipientSearchString);
        }

        public void GoToNextInteraction(string phonenumber)
        {
            var recipientCell = Map.FindUiElementByIdForSpecifiedTime(phonenumber);
            recipientCell.Click();
            Wait();
        }

        public void Validate(TransactionValidationData expectedData)
        {
            ValidateViewHeader(expectedData.ChooseRecipientViewHeader);
        }

        private void ValidateViewHeader(string expectedViewHeader)
        {
            Map.WaitForHeader(expectedViewHeader);
        }
    }
}

﻿using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class DailyLimitInteraction : BaseInteraction
    {
        private readonly KeypadInteraction _keypadInteraction;

        public DailyLimitInteraction(UiElementSearch map) : base(map)
        {
            _keypadInteraction = new KeypadInteraction(map);
        }

        public void InputAmountLimitAndSave(string newAmount)
        {

            //Clear old phonenumber.
            var amountText =
                Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.EnterAmountViewAmountTextBox);
            var deleteDigitButton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypadDelete);
            while (amountText.Text != "")
            {
                deleteDigitButton.Click();
            }

            _keypadInteraction.InputNumberToKeypad(newAmount);

            _keypadInteraction.Next();
        }
    }
}


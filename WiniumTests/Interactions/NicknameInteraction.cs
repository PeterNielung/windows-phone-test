﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class NicknameInteraction : BaseInteraction
    {
        public NicknameInteraction(UiElementSearch map) : base(map)
        {
        }

        public void InputNicknameAndSave(string newNickname)
        {
            var nicknameTextBox =
                Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NicknameViewNicknameTextBox);

            nicknameTextBox.Clear();
            nicknameTextBox.SendKeys(newNickname);
            nicknameTextBox.Submit();
        }

        public void Save()
        {
            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NicknameViewSaveButton).Click();
        }
    }
}

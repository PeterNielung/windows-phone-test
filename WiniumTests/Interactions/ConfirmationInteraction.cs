﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UITest.Common;
using NUnit.Framework;
using OpenQA.Selenium.Remote;
using WiniumTests.TestData;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class ConfirmationInteraction : BaseInteraction
    {
        public ConfirmationInteraction(UiElementSearch map) : base(map)
        {
        }

        public void InputNoteToRecipient(string note)
        {
            var noteTextBox = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.ConfirmationViewNote);
            noteTextBox.SendKeys(note);

            Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.ConfirmationViewName).Click();
        }

        public void CancelYes()
        {
            var cancelButton = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.ConfirmationViewCancelButton);
            cancelButton.Click();

            Wait();
            //Now, a popup with a yes or no should come. We need to select yes her to cancel.
            Map.PopupboxPushAccept();
        }

        public void Accept()
        {
            AcceptSwipeWithSwipperButton();
        }

        private void AcceptSwipeWithSwipperButton()
        {
         
            //Wait for a while, since we need to make sure the keyboard is gone, when we find the element, to get the proper coordiantes
            //(Since the keyboard pushes the swipperbutton up)
            Wait(1000);

            //For some reason, the swipperbutton text is reported as not visible, so we search for it via this method
            var swipperButtonEllipse = Map.FindUiElementForSpecifiedTimeAllowNotVisible(UiElementSearch.UiElement.SwipperButtonEllipse);
            var generalUiInteraction = Map.GeneralUiInteraction();

            //flick right from ellipse coordinates
            generalUiInteraction.Flick(swipperButtonEllipse, swipperButtonEllipse.Size.Width*5, 00, swipperButtonEllipse.Size.Width*3).Perform();

            //wait for API to perform Transaction
            Wait(4000);

            //Sometimes, the Swipe is not being done -- perhaps because a keyboard is present still when it tries.. or just due to lag. 
            //To test that this is not the case, try to Swipe again _iff_ the element still exists. 
            try
            {
                swipperButtonEllipse = Map.FindUiElementForSpecifiedTimeAllowNotVisible(UiElementSearch.UiElement.SwipperButtonEllipse, 1000);
                generalUiInteraction.Flick(swipperButtonEllipse, swipperButtonEllipse.Size.Width * 5, 00, swipperButtonEllipse.Size.Width * 3).Perform();
                Wait(5000);
            }
            catch (Exception)
            {
                Debug.WriteLine("exceptionthrown becase first swipe in Confirmation view Worked");
                //Do nothing, we arrive here if the first swipe worked.
            }
        }

        public void Validate(TransactionValidationData expectedData)
        {
            ValidateViewHeader(expectedData.ConfirmationViewHeader);
            ValidateAmount(expectedData.DisplayAmount);
            ValidateMessage(expectedData.MessageText);
            ValidateRecipient(expectedData.RecipientName, expectedData.RecipientPhoneNumber);
        }

        private void ValidateViewHeader(string expectedViewHeader)
        {
            Map.WaitForHeader(expectedViewHeader);
        }
        private void ValidateRecipient(string expectedName, string expectedPhonenumber)
        {
            var actualName = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.ConfirmationViewName);
            var actualPhoneNumber = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.ConfirmationViewPhoneNumber);

            Assert.AreEqual(expectedName, actualName.Text);
            Assert.AreEqual(expectedPhonenumber, actualPhoneNumber.Text);
        }

        private void ValidateAmount(string expectedAmount)
        {
            var expectedDisplayedAmount = $"{expectedAmount} kr.";
            var actualDisplayedAmount = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.ConfirmationViewAmount);

            Assert.AreEqual(expectedDisplayedAmount, actualDisplayedAmount.Text); 
        }

        private void ValidateMessage(string expectedMessage)
        {
            var actualMessage = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.ConfirmationViewNote);
            Assert.AreEqual(expectedMessage, actualMessage.Text);
        }
    }
}

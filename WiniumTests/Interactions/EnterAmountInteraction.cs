﻿using NUnit.Framework;
using WiniumTests.TestData;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class EnterAmountInteraction : BaseInteraction
    {
        private readonly KeypadInteraction _keypadInteraction;

        public EnterAmountInteraction(UiElementSearch map) : base(map)
        {
            _keypadInteraction = new KeypadInteraction(map);
        }

        public void EnterAmount(string amount)
        {
            _keypadInteraction.InputNumberToKeypad(amount);
        }

        public void GotoNextInteraction()
        {
            _keypadInteraction.Next();
            Wait();
        }

        public void ValidateFail()
        {
            Assert.IsFalse(Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NumberKeypadNext).Enabled);
        }

        public void Validate(TransactionValidationData expectedData)
        {
            ValidateAmount(expectedData.DisplayAmount);
            ValidateHeader(expectedData.EnterAmountViewHeader);
        }

        private void ValidateHeader(string expectedViewHeader)
        {
            Map.WaitForHeader(expectedViewHeader);
        } 
        private void ValidateAmount(string expectedAmount)
        {
            var actualAmount = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.EnterAmountViewAmountTextBox);
            Assert.AreEqual(expectedAmount, actualAmount.Text);
        }

        public string EnterAmountDailyLimitErrorMessage {
            get
            {
                var msg =
                    Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.EnterAmountViewDailyLimitErrorTextBlock);
                return msg.Text;
            }
        }
    }
}

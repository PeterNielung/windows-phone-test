﻿using System.Threading;
using OpenQA.Selenium.Remote;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class BaseInteraction
    {
        protected UiElementSearch Map;

        public BaseInteraction(UiElementSearch map)
        {
            Map = map;
        }

        public void Wait(int ms = 1000)
        {
            //Allow the application to load the next screen by waiting 1 seconds.
            SpinWait.SpinUntil(() => false, ms);
        }

    }
}

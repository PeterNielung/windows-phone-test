﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Swipp.Consumer.Core.TestLibrary.Api.Utilities;
using WiniumTests.Utility;

namespace WiniumTests
{

    public class BrowserInteraction
    {
        private readonly BrowserElementSearch _map;
        public BrowserInteraction(BrowserElementSearch map)
        {
            _map = map;
        }

        public void NavigateTo(string url)
        {
            _map.NavigateTo(url);
            Wait(4000);
        }

        public void InputUseridAndPassword(string userId, string password)
        {
            //switch focus
            _map.SwitchTo(_map.FindBrowserElement(_map.NemIdIframe));

            //input userId and Password
            var userIdBox = _map.FindBrowserElement(_map.UserIdBox);
            var userPasswordBox = _map.FindBrowserElement(_map.UserPasswordBox);

            userIdBox.Click();
            userIdBox.Clear();
            userIdBox.SendKeys(userId);

            userPasswordBox.Click();
            userPasswordBox.Clear();
            userPasswordBox.SendKeys(password);
        }

        public void PressNext()
        {
            _map.FindBrowserElement(_map.NextButton).Click();
            Wait();
        }

        public string GetInputKeyFromCard()
        {
            var cardNumber = _map.FindBrowserElement(_map.CardNumber).Text.Replace("-", string.Empty);
            var keyId = _map.FindBrowserElement(_map.KeyId).Text;

            var credentials = "oces:nemid4all";
            var header = Convert.ToBase64String(credentials.GetBytes());

            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("basic", header);
            var response = Task.Run(async () => await client.GetStringAsync(new Uri("https://appletk.danid.dk/testtools/OtpCard?CardSerial=" + cardNumber))).Result;

            var match = Regex.Match(response, $@"{keyId}.*?(?<nemidkey>\d+)");
            var inputKey = match.Groups["nemidkey"].Value;
            return inputKey;
        }

        public void EnterNemidKey(string key)
        {
            var inputKeyBox = _map.FindBrowserElement(_map.KeyInputBox);

            inputKeyBox.SendKeys(key);
        }

        public void ClickSubmit()
        {
            _map.FindBrowserElement(_map.SubmitButton).Click();
            Wait();
        }

        public string GetCurrentUrl()
        {
            return _map.CurrentUrl();
        }

        private void Wait(int ms = 2000)
        {
            //Allow the application to load the next screen by waiting 1 seconds.
            SpinWait.SpinUntil(() => false, ms);
        }

        public void CloseAndDisposeBrowser()
        {
            _map.CloseAndDispose();
        }


    }
}

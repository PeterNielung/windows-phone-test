﻿using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class TransferAndRequestInteraction : BaseInteraction
    {
        private readonly EnterAmountInteraction _enterAmountInteraction;
        private readonly TransferRequestChooseRecipientInteraction _chooseRecipientInteraction;
        private readonly ConfirmationInteraction _confirmationInteraction;
        private readonly SummaryInteraction _summaryInteraction;
        private readonly ErrorMessageInteraction _errorMessageInteraction;

        public EnterAmountInteraction EnterAmountScreen => _enterAmountInteraction;
        public TransferRequestChooseRecipientInteraction ChooseRecipientScreen => _chooseRecipientInteraction ;
        public ConfirmationInteraction ConfirmationScreen => _confirmationInteraction;
        public SummaryInteraction SummaryScreen => _summaryInteraction;
        public ErrorMessageInteraction ErrorMessageView => _errorMessageInteraction;




        public TransferAndRequestInteraction(UiElementSearch map) : base(map)
        {
            _enterAmountInteraction = new EnterAmountInteraction(map);
            _chooseRecipientInteraction = new TransferRequestChooseRecipientInteraction(map);
            _confirmationInteraction = new ConfirmationInteraction(map);
            _summaryInteraction = new SummaryInteraction(map);
            _errorMessageInteraction = new ErrorMessageInteraction(map);
        }

        public void EnterAmount(string amount)
        {
            _enterAmountInteraction.EnterAmount(amount);
        }
        public void ConfirmEnterAmount()
        {
            _enterAmountInteraction.GotoNextInteraction();
        }

        public void EnterRecipient(string recipient)
        {
            _chooseRecipientInteraction.InputRecipient(recipient);
        }

        public void ConfirmEnterRecipient(string phonenumber)
        {
            _chooseRecipientInteraction.GoToNextInteraction(phonenumber);
        }

        public void EnterTransferOrRequestNote(string note)
        {
            _confirmationInteraction.InputNoteToRecipient(note);
        }

        public void ConfirmTransferOrRequest()
        {
            _confirmationInteraction.Accept();
        }
    }
}

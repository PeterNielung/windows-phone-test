﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using WiniumTests.TestData;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class ErrorMessageInteraction : BaseInteraction
    {
        public ErrorMessageInteraction(UiElementSearch map) : base(map)
        {

        }

        public void Validate(TransactionValidationData expectedData)
        {
            ValidateHeader(expectedData.ErrorMessageViewHeader);
        }

        private void ValidateHeader(string expectedHeader)
        {
            Map.WaitForHeader(expectedHeader);
        }
    }
}

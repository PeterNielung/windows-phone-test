﻿using System.Runtime.InteropServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class MainViewInteraction : BaseInteraction
    {
        public MainViewInteraction(UiElementSearch map) : base(map)
        {
        }

        public void Transfer()
        {
            var button = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainViewTransferButton);
            button.Click();
        }

        public void Payment()
        {
            var button = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainViewPaymentButton);
            button.Click();
        }

        public void Request()
        {
            var button = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainViewRequestButton);
            button.Click();
        }

        public void NotificationBar()
        {
            var button = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainViewNotificationBar);
            button.Click();
        }

        public void Validate(string expectedTranferButtonText, string expectedPaymentButtonText, string expectedRequestButtonText)
        {
            var actualTranferButtonText =
                Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainViewTransferButton).Text;

            var actualPaymentButtonText =
                Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainViewPaymentButton).Text;

            var actualRequestButtonText =
                Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainViewRequestButton).Text;

            Assert.AreEqual(expectedTranferButtonText, actualTranferButtonText);
            Assert.AreEqual(expectedPaymentButtonText, actualPaymentButtonText);
            Assert.AreEqual(expectedRequestButtonText, actualRequestButtonText);
        }

        public void CloseApp()
        {
            Map.PhysicalBackButton();
        }

        //This method works just like the normal notificationbar method. The only difference is that this method, will wait for upto x specified
        //milliseconds, before it gives up clicking the notification. This is useful, if we need to wait for a notification to come, which may take
        //a while.
        public void NotificationBarWaitUpTo(int millisecondsAllowedToSearch = 5000)
        {
            var button = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.MainViewNotificationBar, millisecondsAllowedToSearch);
            button.Click();
        }
    }
}

﻿using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using NUnit.Framework;
using OpenQA.Selenium;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class PaymentChooseRecipientInteraction : BaseInteraction
    {

        public PaymentChooseRecipientInteraction(UiElementSearch map) : base(map)
        {
        }

        public void InputRecipient(string recipientSearchString)
        {
            var recipientSearchText = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.PaymentChooseRecipientSearchString);
            recipientSearchText.SendKeys(recipientSearchString);
        }

        public void GoToNextInteraction()
        {
            var firstRecipientResult =
                Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.PaymentChooseRecipientFirstResultCell);
            firstRecipientResult.Click();
            Wait();
        }

        public void Validate(TestData.TransactionValidationData expectedData)
        {
            ValidateViewHeader(expectedData.ChooseRecipientViewHeader);
        }

        private void ValidateViewHeader(string expectedViewHeader)
        {
            Map.WaitForHeader(expectedViewHeader);
        }

        public void SwitchTab()
        {
            var pivot = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.PaymentChooseRecipientMainPivot);
            var generalUiInteraction = Map.GeneralUiInteraction();
            generalUiInteraction.Flick(pivot, pivot.Size.Width, 0, pivot.Size.Width/2).Perform();
        }

        public string QrToken
        {
            get
            {
                try
                {
                    //Until it has generated the Qr Code, it returns a message such as "indlæser Qr-kode"
                    //We need to wait with returning the Qr Code till it has finished generating it. Thus we allow up to
                    //5 seconds for it to generate. 
                    for (int tries = 0; tries < 5; tries++)
                    {
                        var token =
                        Map.FindUiElementForSpecifiedTime(
                            UiElementSearch.UiElement.PaymentChooseRecipientQrCodeTextBlock);

                        //We assume the Qr code is correct, if it is of the form "xxx xxx xxx xxx". 
                        var regexVerification = new Regex(@"[\w]{3} [\w]{3} [\w]{3} [\w]{3}");
                        if (regexVerification.IsMatch(token.Text))
                        {
                            return token.Text;
                        }
                        Wait(1000);
                    }
                    throw new InvalidElementStateException("Never finished generating Qr code token in time");
                }
                catch
                {
                    //Unable to find the element with the token... Either we are not at the correct view
                    //or at the wrong tab, thus returning an empty string.
                    return "";
                }
            }
        }

    }
}

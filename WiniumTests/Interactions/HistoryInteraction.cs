﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class HistoryInteraction : BaseInteraction
    {
        //TODO: Correct this Class.
        public HistoryInteraction(UiElementSearch map) : base(map)
        {
            
        }

        /// <summary>
        /// This method clicks the row with the matching transaction Id, and throws and error if it did not exist.
        /// Note: This requires that the list items in the History-view has their transaction IDs set as automation IDs
        /// </summary>
        /// <param name="transactionId"></param>
        public void ClickRowWithTransactionId(string transactionId)
        {
            var listItem = Map.FindUiElementByIdForSpecifiedTime(transactionId);
            listItem.Click();
            Wait();
        }

        public void Validate(string exprectedHeader)
        {
            Map.WaitForHeader(exprectedHeader);
        }
    }
}

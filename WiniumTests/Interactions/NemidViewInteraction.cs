﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class NemidViewInteraction : BaseInteraction
    {
        public NemidViewInteraction(UiElementSearch map) : base(map)
        {
        }

        public string GetNemidUrlFromWebView()
        {
            var webview = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NemidWebView);

            var nemidSource = webview.GetAttribute("Source");

            var withoutStart = nemidSource.Trim('"');
            return withoutStart.TrimStart('/');
        }

        public void RedirectAppNemidView(string redirectUrl)
        {
            var webview = Map.FindUiElementForSpecifiedTime(UiElementSearch.UiElement.NemidWebView);
            Map.SetAttribute("Source", webview, redirectUrl);
            Wait(2000);
        }
    }
}



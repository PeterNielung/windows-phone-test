﻿using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class PaymentInteraction : BaseInteraction
    {
        private readonly EnterAmountInteraction _enterAmountInteraction;
        private readonly PaymentChooseRecipientInteraction _chooseRecipientInteraction;
        private readonly ConfirmationInteraction _confirmationInteraction;
        private readonly SummaryInteraction _summaryInteraction;

        public PaymentInteraction(UiElementSearch map) : base(map)
        {
            _enterAmountInteraction = new EnterAmountInteraction(map);
            _chooseRecipientInteraction = new PaymentChooseRecipientInteraction(map);
            _confirmationInteraction = new ConfirmationInteraction(map);
            _summaryInteraction = new SummaryInteraction(map);
        }

#region Properties
        public EnterAmountInteraction EnterAmountScreen => _enterAmountInteraction;
        public PaymentChooseRecipientInteraction ChooseRecipientScreen => _chooseRecipientInteraction;
        public ConfirmationInteraction ConfirmationScreen => _confirmationInteraction;
        public SummaryInteraction SummaryScreen => _summaryInteraction;
#endregion

        public void EnterAmount(string amount)
        {
            _enterAmountInteraction.EnterAmount(amount);
        }
        public void ConfirmEnterAmount()
        {
            _enterAmountInteraction.GotoNextInteraction();
        }

        public void EnterRecipient(string recipient)
        {
            _chooseRecipientInteraction.InputRecipient(recipient);
        }

        public void ConfirmEnterRecipient()
        {
            _chooseRecipientInteraction.GoToNextInteraction();
        }

        public void EnterPaymentNote(string note)
        {
            _confirmationInteraction.InputNoteToRecipient(note);
        }

        public void ConfirmPayment()
        {
            _confirmationInteraction.Accept();
        }

        public void CancelPaymentAtConfirmationScreen()
        {
            _confirmationInteraction.CancelYes();
        }

        public void SwitchTab()
        {
            _chooseRecipientInteraction.SwitchTab();
        }

        public string QrToken {
            get { return _chooseRecipientInteraction.QrToken; }
        }
    }
}

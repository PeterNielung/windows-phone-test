﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WiniumTests.TestData;
using WiniumTests.Utility;

namespace WiniumTests.Interactions
{
    public class PinInteraction : BaseInteraction
    {
        public PinInteraction(UiElementSearch map) : base(map)
        {
        }

        public void InputPin(string pin)
        {
            var keypad = new KeypadInteraction(Map);
            keypad.InputNumberToKeypad(pin);
        }

        public void PressNext()
        {
            var keypad = new KeypadInteraction(Map);
            keypad.Next();
        }

        //public void Validate(SettingsValidationData expectedData)
        //{
           
        //}

        public void ValidateHeader(string expectedHeader)
        {
            Map.WaitForHeader(expectedHeader);
        }
    }
}

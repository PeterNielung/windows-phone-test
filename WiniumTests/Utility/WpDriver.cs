﻿using System;
using System.Reflection;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace WiniumTests.Utility
{
    public class WpDriver : RemoteWebDriver, IHasTouchScreen
    {
        private ITouchScreen _touchScreen;
        
        #region Constructors and Destructors
        public WpDriver(ICommandExecutor commandExecutor, ICapabilities desiredCapabilities)
            : base(commandExecutor, desiredCapabilities)
        {
        }

        public WpDriver(ICapabilities desiredCapabilities)
            : base(desiredCapabilities)
        {
        }

        public WpDriver(Uri remoteAddress, ICapabilities desiredCapabilities)
            : base(remoteAddress, desiredCapabilities)
        {
        }

        public WpDriver(Uri remoteAddress, ICapabilities desiredCapabilities, TimeSpan commandTimeout)
            : base(remoteAddress, desiredCapabilities, commandTimeout)
        {
        }

        #endregion

        #region Public Properties

        public ITouchScreen TouchScreen
        {
            get
            {
                return this._touchScreen ?? (this._touchScreen = new RemoteTouchScreen(this));
            }
        }

        #endregion

        private static object Invoke(object o, string methodName, params object[] args)
        {
            var mi = o.GetType().GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Instance);
            if (mi != null)
            {
                return mi.Invoke(o, args);
            }
            return null;
        }

        public IWebElement FindByXName(string locator)
        {
            return Invoke(this, "FindElement", "xname", locator) as IWebElement;
        }

        public static IWebElement FindByXName(IWebElement root, string locator)
        {
            var parentElement = root as RemoteWebElement;
            return Invoke(parentElement, "FindElement", "xname", locator) as IWebElement;
        }

        public void PopupBox_Accept()
        {
            Execute(DriverCommand.AcceptAlert, null);
        }

        public void PopupBox_Cancel()
        {
            Execute(DriverCommand.DismissAlert, null);
        }

        public void PopupBox_Ok()
        {
            Execute(DriverCommand.AcceptAlert, null);
        }
    }
}
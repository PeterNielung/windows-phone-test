﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UITest.Common;
using NUnit.Framework.Compatibility;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Remote;
using WiniumTests.Tests;

namespace WiniumTests.Utility
{
    /// <summary>
    /// This class provides helpermethods for finding UI elements, based on their automation ID.
    /// </summary>
    public class UiElementSearch
    {
        //Enum with all UI elements
        public enum UiElement
        {
            //General Elements
            BackBaseHeaderViewText,
            MenuBaseHeaderViewText,
            SendSMSButton,

            //EnrollmentElements
            StartButton,
            BankViewSearchBox,
            AcceptTermsCheckBox,
            AcceptTermsAcceptButton,
            NemidWebView,

            //ErrorMessageView
            ErrorMessageViewMessageText,

            //Keypad
            NumberKeypad0,
            NumberKeypad1,
            NumberKeypad2,
            NumberKeypad3,
            NumberKeypad4,
            NumberKeypad5,
            NumberKeypad6,
            NumberKeypad7,
            NumberKeypad8,
            NumberKeypad9,
            NumberKeypadDeleteComma,
            NumberKeypadNext,
            NumberKeypadDelete,

            //Mainview
            MainViewTransferButton,
            MainViewPaymentButton,
            MainViewRequestButton,
            MainViewNotificationBar,

            //Mainmenu
            MainMenuShowMenu,
            MainMenuCloseMenu,
            MainMenuSwipp,
            MainMenuHistory,
            MainMenuSettings,
            MainMenuTerms,
            MainMenuHelp,
            MainMenuLogout,

            //MessageView
            MessageViewConfirmButton,

            //Enter Amount View
            EnterAmountViewAmountTextBox,
            EnterAmountViewHeader,
            EnterAmountErrorMessage,
            EnterAmountViewDailyLimitErrorTextBlock,

            //Transfer and Request, Choose Recipient View
            TransferRequestChooseRecipientSearchTextBox,
            TransferRequestChooseRecipientFirstResultCell,
            TransferRequestChooseRecipientHeader,
            TransferRequestChooseRecipientName,

            //Confirmation view
            ConfirmationViewHeader,
            ConfirmationViewAmount,
            ConfirmationViewFee,
            ConfirmationViewName,
            ConfirmationViewPhoneNumber,
            ConfirmationViewNote,
            ConfirmationViewCancelButton,

            //SwipperButton
            SwipperButtonEllipse,

            //Payment Choose Recipient View
            PaymentChooseRecipientSearchString,
            PaymentChooseRecipientFirstResultCell,
            PaymentChooseRecipientQrCodeTextBlock,
            PaymentChooseRecipientMainPivot,

            //PinView
            PinViewInfoMessage,

            //Summary View
            SummaryViewHeader,
            SummaryViewAmount,
            SummaryViewFee,
            SummaryViewHeaderText,
            SummaryViewName,
            SummaryViewPhoneNumber,
            SummaryViewNote,
            SummaryViewTransactionTimeSection,
            SummaryViewTransactionId,
            SummaryViewCloseButton,

            //Settings View
            SettingsViewNicknameButton,
            SettingsViewNicknameButtonValue,
            SettingsViewMsisdnButton,
            SettingsViewMsisdnButtonValue,
            SettingsViewPinButton,
            SettingsViewPinButtonValue,
            SettingsViewEmailButton,
            SettingsViewEmailButtonValue,
            SettingsViewBankButton,
            SettingsViewBankButtonValue,
            SettingsViewBankAccountButton,
            SettingsViewBankAccountButtonValue,
            SettingsViewAmountLimitButton,
            SettingsViewAmountLimitButtonValue,
            SettingsViewSoundToggleSwitch,
            SettingsViewSoundToggleSwitchValue,
            SettingsViewVersionButton,
            SettingsViewVersionButtonValue,
            SettingsViewBlockAgreementButton,
            SettingsViewBlockAgreementButtonValue,
            SettingsViewDeleteAgreementButton,
            SettingsViewDeleteAgreementButtonValue,

            //Phonenumber View
            PhoneNumberViewCountryCodeButton,
            PhoneNumberViewCurrentCountryCode,
            PhoneNumberViewPhonenumber,

            //Nickname View
            NicknameViewNicknameTextBox,
            NicknameViewSaveButton,

            //Email View
            EmailViewEmailInputTextBox,
            EmailViewMarketingToggleSwitch,
            EmailViewSaveChangesButton,
        }

        //Fields
        private readonly WpDriver _driver;
        private Dictionary<UiElement, string> _automationIdMap;
        

        public UiElementSearch(WpDriver driver)
        {
            _driver = driver;
            InitializeIdMap();
        }

        private void InitializeIdMap()
        {
            _automationIdMap = new Dictionary<UiElement, string>()
            {
                //generalElements
                { UiElement.BackBaseHeaderViewText, "TitleText" }, //Every BackBaseView Header
                { UiElement.MenuBaseHeaderViewText, "TitleTextBox" },
                { UiElement.SendSMSButton, "SendSMSButton" },

                //Enrollment Elements
                { UiElement.StartButton, "StartButton" },
                { UiElement.BankViewSearchBox, "SearchBox" },
                { UiElement.AcceptTermsCheckBox, "AcceptTermsCheckBox" },
                { UiElement.AcceptTermsAcceptButton, "SaveButton" },
                { UiElement.NemidWebView, "WebView" },

                //ErrorMessageView
                { UiElement.ErrorMessageViewMessageText , "ErrorMessageText"},

                //Keypad
                { UiElement.NumberKeypad0,    "SwippKeypad0" },
                { UiElement.NumberKeypad1,    "SwippKeypad1" },
                { UiElement.NumberKeypad2,    "SwippKeypad2" },
                { UiElement.NumberKeypad3,    "SwippKeypad3" },
                { UiElement.NumberKeypad4,    "SwippKeypad4" },
                { UiElement.NumberKeypad5,    "SwippKeypad5" },
                { UiElement.NumberKeypad6,    "SwippKeypad6" },
                { UiElement.NumberKeypad7,    "SwippKeypad7" },
                { UiElement.NumberKeypad8,    "SwippKeypad8" },
                { UiElement.NumberKeypad9,    "SwippKeypad9" },
                { UiElement.NumberKeypadDeleteComma,    "SwippKeypadButtonDeleteComma" },
                { UiElement.NumberKeypadNext,     "ButtonNext" },
                { UiElement.NumberKeypadDelete, "DeleteButton"},

                //Main view
                { UiElement.MainViewTransferButton,    "SwippTransferButton" },
                { UiElement.MainViewPaymentButton,     "SwippPayButton" },
                { UiElement.MainViewRequestButton,     "SwippRequestButton" },
                { UiElement.MainViewNotificationBar,   "MainViewNotificationBar" },

                //Main menu
                { UiElement.MainMenuShowMenu,      "MainMenu_ShowMenu" },
                { UiElement.MainMenuCloseMenu,     "MainMenu_HideMenu" },
                { UiElement.MainMenuSwipp,         "MainMenu_ButtonSwipp" },
                { UiElement.MainMenuHistory,       "MainMenu_History" },
                { UiElement.MainMenuSettings,      "MainMenu_Settings" },
                { UiElement.MainMenuTerms,         "MainMenu_Terms" },
                { UiElement.MainMenuHelp,          "MainMenu_Help" },
                { UiElement.MainMenuLogout,        "MainMenu_Logout" },

                //MessageView
                { UiElement.MessageViewConfirmButton, "Button" },

                //Enter Amount View
                { UiElement.EnterAmountViewAmountTextBox,   "AmountControlDisplayString"},
                { UiElement.EnterAmountErrorMessage, "ErrorMessageTextBlock" },
                { UiElement.EnterAmountViewDailyLimitErrorTextBlock, "EnterAmountViewDailyLimitErrorTextBlock" },
                
                //Transfer and Request, Choose Recipient View
                { UiElement.TransferRequestChooseRecipientSearchTextBox,    "ChooseRecipientSearchTextBox"},
                { UiElement.TransferRequestChooseRecipientFirstResultCell,  "ChooseRecViewManualListView" },
                { UiElement.TransferRequestChooseRecipientName, "" },

                //Confirmation view
                { UiElement.ConfirmationViewCancelButton, "CancelTransactionButton" },
                { UiElement.ConfirmationViewAmount, "ConfirmationViewAmount" },
                { UiElement.ConfirmationViewFee, "ConfirmationViewFeeAmount" },
                { UiElement.ConfirmationViewName, "ConfirmationViewNickname" },
                { UiElement.ConfirmationViewPhoneNumber, "ConfirmationViewMsisdn" },
                { UiElement.ConfirmationViewNote, "ConfirmationViewNoteTextBox" },

                //swipperButton
                { UiElement.SwipperButtonEllipse, "AcceptSwipperButtonEllipse" },

                //Payment Choose Recipient View
                { UiElement.PaymentChooseRecipientSearchString, "ChoosePRecViewSearchTextBox" },
                { UiElement.PaymentChooseRecipientFirstResultCell, "ChoosePRecViewEnteredListView" },
                { UiElement.PaymentChooseRecipientQrCodeTextBlock, "PaymentChooseRecipientQrCodeTextBlock" },
                { UiElement.PaymentChooseRecipientMainPivot, "PaymentChooseRecipientMainPivot" },

                //PinView
                {UiElement.PinViewInfoMessage, "InfoMessageTextBlock" },

                //Summary View
                { UiElement.SummaryViewAmount, "SummaryViewAmount" },
                { UiElement.SummaryViewFee, "SummaryViewFee" },
                { UiElement.SummaryViewHeaderText, "SummaryViewVmText" },
                { UiElement.SummaryViewName, "SummaryViewNicknameBlock" },
                { UiElement.SummaryViewPhoneNumber, "SummaryViewMsisdnBlock" },
                { UiElement.SummaryViewNote, "SummaryViewNoteText" },
                { UiElement.SummaryViewTransactionTimeSection, "SummaryViewTransactionTimeSection" },
                { UiElement.SummaryViewTransactionId, "SummaryViewTransactionIdText" },
                { UiElement.SummaryViewCloseButton, "CancelTransactionButton" },

                //Settings View
                { UiElement.SettingsViewNicknameButton,             "SettingsViewNicknameButton" },
                { UiElement.SettingsViewNicknameButtonValue,        "SettingsViewNicknameButtonValue" },
                { UiElement.SettingsViewMsisdnButton,               "SettingsViewMsisdnButton" },
                { UiElement.SettingsViewMsisdnButtonValue,          "SettingsViewMsisdnButtonValue" },
                { UiElement.SettingsViewPinButton,                  "SettingsViewPinButton" },
                { UiElement.SettingsViewPinButtonValue,             "SettingsViewPinButtonValue" },
                { UiElement.SettingsViewEmailButton,                "SettingsViewEmailButton" },
                { UiElement.SettingsViewEmailButtonValue,           "SettingsViewEmailButtonValue" },
                { UiElement.SettingsViewBankButton,                 "SettingsViewBankButton" },
                { UiElement.SettingsViewBankButtonValue,            "SettingsViewBankButtonValue" },
                { UiElement.SettingsViewBankAccountButton,          "SettingsViewBankAccountButton" },
                { UiElement.SettingsViewBankAccountButtonValue,     "SettingsViewBankAccountButtonValue" },
                { UiElement.SettingsViewAmountLimitButton,          "SettingsViewAmountLimitButton" },
                { UiElement.SettingsViewAmountLimitButtonValue,     "SettingsViewAmountLimitButtonValue" },
                { UiElement.SettingsViewSoundToggleSwitch,          "SettingsViewSoundToggleSwitch" },
                { UiElement.SettingsViewSoundToggleSwitchValue,     "SettingsViewSoundToggleSwitchValue" },
                { UiElement.SettingsViewVersionButton,              "SettingsViewVersionButton" },
                { UiElement.SettingsViewVersionButtonValue,         "SettingsViewVersionButtonValue" },
                { UiElement.SettingsViewBlockAgreementButton,       "SettingsViewBlockAgreementButton" },
                { UiElement.SettingsViewBlockAgreementButtonValue,  "SettingsViewBlockAgreementButtonValue" },
                { UiElement.SettingsViewDeleteAgreementButton,      "SettingsViewDeleteAgreementButton" },
                { UiElement.SettingsViewDeleteAgreementButtonValue, "SettingsViewDeleteAgreementButtonValue" },

                //Phonenumber view
                { UiElement.PhoneNumberViewCountryCodeButton, "PhoneNumberViewCountryCodeButton" },
                { UiElement.PhoneNumberViewCurrentCountryCode, "PhoneNumberViewCurrentCountryCode" },
                { UiElement.PhoneNumberViewPhonenumber, "PhoneNumberViewPhonenumber" },
                
                //Nickname View
                { UiElement.NicknameViewNicknameTextBox, "NicknameViewNicknameTextBox" },
                { UiElement.NicknameViewSaveButton,      "NicknameViewSaveButton" },
                
                //Email View
                { UiElement.EmailViewEmailInputTextBox,     "EmailViewEmailInputTextBox" },
                { UiElement.EmailViewMarketingToggleSwitch, "EmailViewMarketingToggleSwitch" },
                { UiElement.EmailViewSaveChangesButton,      "EmailViewSaveChangesButton" }
                
            };
        }

        /// <summary>
        /// Private, since it should not be used. FindUiElementForSpecifiedTime is safer.
        /// Keeping it, as it might have use later.
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        private IWebElement FindUiElement(UiElement element)
        {
            

            var automationId = _automationIdMap[element];
            var foundElement = _driver.FindElementById(automationId);

            //If automation id is "" it just finds first element, so we need to check for both cases
            if (automationId == "")
            {
                throw new InvalidEnumArgumentException("Element for Enum was not found");
            }

            return foundElement;
        }

        //Keeps trying to find an element for a specified amount of time. Surpresses exceptions that arises from trying to find it, until
        //a specified amount of time has passed.
        public IWebElement FindUiElementForSpecifiedTime(UiElement element, int milliSecondsToSearchForElement = 5000)
        {
            var sw = new Stopwatch();
            sw.Start();

            //First see if we know the automationID for the element:
            var automationId = _automationIdMap[element];
            if (automationId == "")
            {
                //We need to watch for this, as it will find _any_ element with a automationsearchstring of ""
                throw new InvalidEnumArgumentException("Element for enum value was not found");
            }

            //Keep searching for element for duration
            while (sw.ElapsedMilliseconds < milliSecondsToSearchForElement)
            {
                try
                {
                    //This will throw an exception, if it does not exist
                    var foundElement = _driver.FindElementById(automationId);

                    //We can find the elements, before they are done loading and thus enabled and visible
                    //We need to protect against finding them _too fast_.
                    if (foundElement.Displayed)
                    {
                        return foundElement;
                    }

                }
                catch
                {
                }

                //Wait for 200ms (random number chosen) instead of spamming the webdriver.
                SpinWait.SpinUntil(() => false, 200);
            }

            throw new NotFoundException("Automation ID: " + automationId.ToString());
        }

        public void WaitForHeader(string headerText, int maxSecondsToWait = 5)
        {
            for (var i = 0; i < maxSecondsToWait; i++)
            {
                //Check the headertextbox, when there is no menu
                try
                {
                    var header = FindUiElementForSpecifiedTime(UiElement.BackBaseHeaderViewText, 250);
                    if (header.Text.Equals(headerText))
                        break;
                }
                catch(Exception) {}

                //Check the different headertextbox, when there is a menu
                try
                {
                    var headerWhenMenuIsPresent = FindUiElementForSpecifiedTime(UiElement.MenuBaseHeaderViewText, 250);
                    if (headerWhenMenuIsPresent.Text.Equals(headerText))
                    {
                        break;
                    }
                }
                catch (Exception) {}
                
                if (i == maxSecondsToWait - 1)
                {
                    throw new NotFoundException("Header never came");
                }

                SpinWait.SpinUntil(() => false, 500);
            }
        }

        //Some elements, including the text on the swipperbutton, is always "not visible". This method allows for 
        //Finding these too for a specified time. 
        public IWebElement FindUiElementForSpecifiedTimeAllowNotVisible(UiElement element, int milliSecondsToSearchForElement = 5000)
        {
            var sw = new Stopwatch();
            sw.Start();

            //First see if we know the automationID for the element:
            var automationId = _automationIdMap[element];
            if (automationId == "")
            {
                //We need to watch for this, as it will find _any_ element with a automationsearchstring of ""
                throw new InvalidEnumArgumentException("Element for enum value was not found");
            }

            //Keep searching for element for duration
            while (sw.ElapsedMilliseconds < milliSecondsToSearchForElement)
            {
                try
                {
                    //This will throw an exception, if it does not exist
                    return _driver.FindElementById(automationId);
                }
                catch
                {
                }

                //Wait for 200ms (random number chosen) instead of spamming the webdriver.
                SpinWait.SpinUntil(() => false, 200);
            }

            throw new NotFoundException("Automation ID: " + automationId.ToString());
        }

        public IWebElement FindUiElementByIdForSpecifiedTime(string automationId, int milliSecondsToSearchForElement = 5000)
        {
            var sw = new Stopwatch();
            sw.Start();

            //First see if we know the automationID for the element:
            if (string.IsNullOrWhiteSpace(automationId))
            {
                //We need to watch for this, as it will find _any_ element with a automationsearchstring of ""
                throw new ArgumentNullException($"{nameof(automationId)} is null or Empty");
            }

            //Keep searching for element for duration
            while (sw.ElapsedMilliseconds < milliSecondsToSearchForElement)
            {
                try
                {
                    //This will throw an exception, if it does not exist
                    var foundElement = _driver.FindElementById(automationId);

                    //We can find the elements, before they are done loading and thus enabled and visible
                    //We need to protect against finding them _too fast_.
                    if (foundElement.Displayed)
                    {
                        return foundElement;
                    }

                }
                catch
                {
                }

                //Wait for 200ms (random number chosen) instead of spamming the webdriver.
                SpinWait.SpinUntil(() => false, 200);
            }

            throw new NotFoundException("Automation ID: " + automationId.ToString());
        }

        /*
        public ReadOnlyCollection<IWebElement> FindUiElementByClassnameForSpecifiedTime(string classname, int milliSecondsToSearchForElement = 5000)
        {
            var sw = new Stopwatch();
            sw.Start();

            //First see if we know the automationID for the element:
            if (string.IsNullOrWhiteSpace(classname))
            {
                //We need to watch for this, as it will find _any_ element with a automationsearchstring of ""
                throw new ArgumentNullException($"{nameof(classname)} is null or Empty");
            }

            //Keep searching for element for duration
            while (sw.ElapsedMilliseconds < milliSecondsToSearchForElement)
            {
                try
                {
                    //This will throw an exception, if it does not exist
                    var foundElement = _driver.FindElementsByClassName(classname);

                    return foundElement;

                }
                catch
                {
                }

                //Wait for 200ms (random number chosen) instead of spamming the webdriver.
                SpinWait.SpinUntil(() => false, 200);
            }

            throw new NotFoundException("Automation ID: " + classname.ToString());
        }
        */

        public TouchActions GeneralUiInteraction()
        {
            return new TouchActions(_driver);
        }

        public void PhysicalBackButton()
        {
            _driver.Navigate().Back();
        }

        public void PopupboxPushAccept()
        {
            _driver.PopupBox_Accept();
        }

        public void PopupboxPushDismiss()
        {
            _driver.PopupBox_Cancel();
        }

        public void PopupboxPushOk()
        {
            _driver.PopupBox_Ok();
        }

        public void SetAttribute(string attributeName, IWebElement webElement, string value)
        {
            _driver.ExecuteScript("attribute: set", webElement, attributeName, value);
        }

        public void ScrollToDeleteButton()
        {
           var actions = new TouchActions(_driver);

            actions.ClickAndHold(FindUiElementForSpecifiedTime(UiElement.SettingsViewBankButton));
            actions.MoveToElement(FindUiElementForSpecifiedTime(UiElement.MenuBaseHeaderViewText));
            actions.Release();
            actions.Perform();
        }
    }
}

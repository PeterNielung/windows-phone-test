﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using NUnit.Framework.Constraints;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using WiniumTests.TestData;

namespace WiniumTests.Utility
{
    public static class SetupTeardown
    {
        private static WpDriver _driver;
        public static UiElementSearch SetupUpApp()
        {
            var setupData = new SetupData();

            var dc = new DesiredCapabilities(new Dictionary<string, object>()
                {
                    { "debugConnectToRunningApp", true },
                    { "app",setupData.UsedAppxFile },
                    { "deviceName", setupData.Win81Device }
                }
            );

            _driver = new WpDriver(new Uri("http://localhost:9999"), dc);
            return new UiElementSearch(_driver);
        }

        public static BrowserElementSearch SetupBrowser()
        {
            string path = Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)).FullName;
            path += "\\Roaming\\Mozilla\\Firefox\\Profiles\\";
            var profileDirectory = Directory.GetDirectories(path, "*.default").ToString();
            
            var profile = new FirefoxProfile(profileDirectory);
            var driver = new FirefoxDriver(new FirefoxBinary("C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"), profile);
            return new BrowserElementSearch(driver);
        }

        public static void TearDown()
        {
            //Navigates back until app closes
            _driver.Navigate().Back();
            SpinWait.SpinUntil(() => false, 100);
            _driver.Navigate().Back();
            SpinWait.SpinUntil(() => false, 100);
            _driver.Navigate().Back();
            SpinWait.SpinUntil(() => false, 100);
            _driver.Navigate().Back();
            SpinWait.SpinUntil(() => false, 100);
            _driver.Navigate().Back();
            SpinWait.SpinUntil(() => false, 100);
            _driver.Navigate().Back();
        }
    }
}

﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace WiniumTests.Utility
{
    /// <summary>
    /// This class provides helpermethods for finding Browser elements.
    /// </summary>
    public class BrowserElementSearch
    {
        //Fields
        private readonly IWebDriver _driver;

        //elements
        public BrowserElement NemIdIframe { get; private set; }
        public BrowserElement UserIdBox { get; private set; }
        public BrowserElement UserPasswordBox { get; private set; }
        public BrowserElement NextButton { get; private set; }
        public BrowserElement CardNumber { get; private set; }
        public BrowserElement KeyId { get; private set; }
        public BrowserElement KeyInputBox { get; private set; }
        public BrowserElement SubmitButton { get; private set; }
 
        public BrowserElementSearch(IWebDriver driver)
        {
            _driver = driver;
            InitializeIdMap();
        }

        private void InitializeIdMap()
        {
            NemIdIframe = new BrowserElement(BrowserElementSearchType.Id, "nemid_iframe");
            UserIdBox = new BrowserElement(BrowserElementSearchType.CssSelector, "input[aria-labeledby='userid-label']");
            UserPasswordBox = new BrowserElement(BrowserElementSearchType.CssSelector, "input[type='password']");
            NextButton = new BrowserElement(BrowserElementSearchType.CssSelector, "button[title='Fortsæt til næste skærmbillede']");
            CardNumber = new BrowserElement(BrowserElementSearchType.CssSelector, "span[class='otp__card-number']");
            KeyId = new BrowserElement(BrowserElementSearchType.ClassName, "otp__frame__cell");
            KeyInputBox = new BrowserElement(BrowserElementSearchType.CssSelector, "input[aria-label='Indtast nøgle']");
            SubmitButton = new BrowserElement(BrowserElementSearchType.ClassName, "button--submit");
        }

        public IWebElement FindBrowserElement(BrowserElement element, int maxSecondsToSearchForBrowser = 7)
        {
            var searchId = element.SearchId;
            var searchType = element.SearchType;
            IWebElement foundElement = null;

            //First see if we know the searchId for the element:
            if (string.IsNullOrWhiteSpace(searchId))
            {
                //We need to watch for this, as it will find _any_ element with a automationsearchstring of ""
                throw new ArgumentNullException($"{nameof(searchId)} is null or Empty");
            }

            for (var tries = 0; tries < maxSecondsToSearchForBrowser; tries++)
            {
                switch (searchType)
                {
                    case BrowserElementSearchType.Id:
                        foundElement = _driver.FindElements(By.Id(searchId)).FirstOrDefault(e => e.Displayed);
                        break;
                    case BrowserElementSearchType.ClassName:
                        foundElement = _driver.FindElements(By.ClassName(searchId)).FirstOrDefault(e => !string.IsNullOrWhiteSpace(e.Text) && e.Displayed);
                        break;
                    case BrowserElementSearchType.CssSelector:
                        foundElement = _driver.FindElements(By.CssSelector(searchId)).FirstOrDefault(e => e.Displayed);
                        break;
                    default:
                        throw new ArgumentException($"Not supported SearchType: {searchType}");
                }

                if (foundElement != null)
                {
                    //We found the browser
                    return foundElement;
                }

                SpinWait.SpinUntil(() => false, 1000);
            }
            throw new NoSuchElementException("Browser never found");
        }

        public void SwitchTo(IWebElement element)
        {
            _driver.SwitchTo().Frame(element);
        }

        public void NavigateTo(string url)
        {
            _driver.Navigate().GoToUrl(url);
        }

        public string CurrentUrl()
        {
            return _driver.Url;
        }

        public void CloseAndDispose()
        {
            Task.Run(() => _driver.Quit()).Wait();
            Task.Run(() => _driver.Dispose()).Wait();
        }
    }//end class

    public class BrowserElement
    {
        public BrowserElement(BrowserElementSearchType searchType, string searchId)
        {
            SearchType = searchType;
            SearchId = searchId;
        }
        public BrowserElementSearchType SearchType { get; }
        public string SearchId { get; }
    }

    public enum BrowserElementSearchType
    {
        Id,
        ClassName,
        CssSelector
    }
}

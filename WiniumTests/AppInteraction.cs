﻿using System;
using WiniumTests.Interactions;
using WiniumTests.Utility;

namespace WiniumTests
{
    public class AppInteraction
    {
        private readonly UiElementSearch _map;

        public AppInteraction(UiElementSearch map)
        {
            _map = map;

            MainViewInteraction = new MainViewInteraction(map);
            LoginInteraction = new LoginInteraction(map);
            PaymentInteraction = new PaymentInteraction(map);
            TransferAndRequestInteraction = new TransferAndRequestInteraction(map);
            MainMenuInteractionInteraction = new SwippMainMenuInteraction(map);
            HistoryInteraction = new HistoryInteraction(map);
            SettingsInteraction = new SettingsInteraction(map);
            EnrollmentInteraction = new EnrollmentInteraction(map);
            
        }

        public void PhysicalBackButton()
        {
            _map.PhysicalBackButton();
        }

        public TransferAndRequestInteraction TransferAndRequestInteraction { get; }

        public MainViewInteraction MainViewInteraction { get; }

        public SwippMainMenuInteraction MainMenuInteractionInteraction { get; }

        public PaymentInteraction PaymentInteraction { get; }
        
        public LoginInteraction LoginInteraction { get; }

        public HistoryInteraction HistoryInteraction { get; }

        public SettingsInteraction SettingsInteraction { get; }

        public EnrollmentInteraction EnrollmentInteraction { get; }

    }
}
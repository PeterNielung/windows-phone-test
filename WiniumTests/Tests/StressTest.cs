﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Converters;
using NUnit.Framework;
using WiniumTests.TestData;
using WiniumTests.Utility;

namespace WiniumTests.Tests
{
    [TestFixture]
    public class StressTest
    {
        private AppInteraction _appInteraction;
        //private BrowserInteraction _browserInteraction;
        private List<TestDataSet> _testData;

        #region Setup
        [OneTimeSetUp]
        public void PrepareDataSets()
        {
            //Initialize testData
            _testData = TestDataFactory.GetTestDataSets(0);
        }

        [SetUp]
        public void SetupApp()
        {
            _appInteraction = new AppInteraction(SetupTeardown.SetupUpApp());

            Login();
        }

        private void Login()
        {
            var credentials = _testData.FirstOrDefault().LoginInputData;

            //Assumes we are at the loginscreen
            var loginInteraction = _appInteraction.LoginInteraction;
            loginInteraction.Login(credentials.Pin);
        }


        #endregion

        [Test]
        public void TestSettings()
        {
            var testData = _testData.FirstOrDefault(t => t.TestDataId.Equals("ST-28"));
            //NickName
            PerformSettingsTest(testData, TestArea.Nickname);
            _appInteraction.MainViewInteraction.Validate("Overfør", "Betal", "Anmod");
            //Msisdn
            PerformSettingsTest(testData, TestArea.Msisdn);
            _appInteraction.MainViewInteraction.Validate("Overfør", "Betal", "Anmod");
            //Pin
            PerformSettingsTest(testData, TestArea.Pin);
            _appInteraction.MainViewInteraction.Validate("Overfør", "Betal", "Anmod");
            //Email
            PerformSettingsTest(testData, TestArea.Email);
            _appInteraction.MainViewInteraction.Validate("Overfør", "Betal", "Anmod");
            //DailyLimit
            PerformSettingsTest(testData, TestArea.DailyLimit);
            _appInteraction.MainViewInteraction.Validate("Overfør", "Betal", "Anmod");
            //Bank
            PerformSettingsTest(testData, TestArea.Bank);
            _appInteraction.MainViewInteraction.Validate("Overfør", "Betal", "Anmod");
            //Account
            PerformSettingsTest(testData, TestArea.Account);
            _appInteraction.MainViewInteraction.Validate("Overfør", "Betal", "Anmod");
        }

        [Test]
        public void TestMainToSettings()
        {
            var testData = _testData.FirstOrDefault(t => t.TestDataId.Equals("ST-28"));
            for (int i = 0; i < 10; i++)
            {
                SpinWait.SpinUntil(() => false, 1000);
                OpenSettingsViaMenu(testData);
                SpinWait.SpinUntil(() => false, 1000);
                _appInteraction.PhysicalBackButton();
            }
            SpinWait.SpinUntil(() => false, 1000);
            _appInteraction.MainViewInteraction.Validate("Overfør", "Betal", "Anmod");
        }

        private void PerformSettingsTest(TestDataSet testData, TestArea testArea)
        {
            //Open settings
            OpenSettingsViaMenu(testData);
            for (int i = 0; i < 10; i++)
            {
                SpinWait.SpinUntil(() => false, 1000);
                var settingsInteraction = _appInteraction.SettingsInteraction;
                SpinWait.SpinUntil(() => false, 1000);
                switch (testArea)
                {
                        case TestArea.Nickname:
                        settingsInteraction.StartNicknameInteraction();
                        SpinWait.SpinUntil(() => false, 1000);
                        _appInteraction.PhysicalBackButton();
                        break;
                        case TestArea.Msisdn:
                        settingsInteraction.StartPhoneNumberInteraction();
                        break;
                        case TestArea.Pin:
                        settingsInteraction.StartPinInteraction();
                        break;
                        case TestArea.Email:
                        settingsInteraction.StartEmailInteraction();
                        SpinWait.SpinUntil(() => false, 1000);
                        _appInteraction.PhysicalBackButton();
                        break;
                        case TestArea.DailyLimit:
                        settingsInteraction.StartDailyLimitInteraction();
                        break;
                        case TestArea.Bank:
                        settingsInteraction.PressChangeBankButton();
                        break;
                        case TestArea.Account:
                        settingsInteraction.PressChangeAccountButton();
                        SpinWait.SpinUntil(() => false, 1000);
                        break;
                }
                SpinWait.SpinUntil(() => false, 1000);
                _appInteraction.PhysicalBackButton();
            }
            SpinWait.SpinUntil(() => false, 1000);
            _appInteraction.PhysicalBackButton();
        }

        private void OpenSettingsViaMenu(TestDataSet testData)
        {
            //Is the menu accessible?
            var menu = _appInteraction.MainMenuInteractionInteraction;
            Assert.AreEqual(menu.IsMenuAccessible, true);

            //Open settings
            menu.ShowMenu();
            menu.Settings();
            _appInteraction.SettingsInteraction.Validate(testData.SettingsValidationData.SettingsViewHeader);
        }

        public enum TestArea
        {
            Nickname,
            Msisdn,
            Pin,
            Email,
            DailyLimit,
            Bank,
            Account
        }

        [TearDown]
        public void TearDown()
        {
            SetupTeardown.TearDown();
        }
    }
}

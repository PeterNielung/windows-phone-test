﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Media.Animation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using WiniumTests.Interactions;
using WiniumTests.TestData;
using WiniumTests.Utility;
using Assert = NUnit.Framework.Assert;

namespace WiniumTests.Tests
{
    [TestFixture]
    class SettingsTest
    {

        private bool _passTestswithNoTestData = false;
        private AppInteraction _appInteraction;
        private List<TestDataSet> _testData;

        #region SetupAndLogin

        [OneTimeSetUp]
        public void PrepareDataSets()
        {
            //Initialize testData
            _testData = TestDataFactory.GetTestDataSets(0);
        }

        [SetUp]
        public void SetupApp()
        {
            _appInteraction = new AppInteraction(SetupTeardown.SetupUpApp());

            Login();
        }

        private void Login()
        {
            var credentials = _testData.FirstOrDefault().LoginInputData;

            //Assumes we are at the loginscreen
            var loginInteraction = _appInteraction.LoginInteraction;
            loginInteraction.Login(credentials.Pin);
        }

        #endregion

        /// <summary>
        /// Change Nickname
        /// </summary>
        [Test]
        public void ST_28()
        {
            var settingsTestData = _testData.Where(t => t.TestDataId.Equals("ST-28"));
            foreach (var testData in settingsTestData)
            {
                PerformChangeNickname(testData);
            }

            if (settingsTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        //Change daily account limit
        public void ST_443()
        {
            var testRuns = 0;
            var settingsTestData = _testData.Where(t => t.TestDataId.Equals("ST-443"));
            foreach (var testData in settingsTestData)
            {
                testRuns++;
                PerformChangeNickname(testData);
            }

            if (testRuns != 0) return;
            if (!_passTestswithNoTestData)
            {
                throw new Exception("No testdata for test");
            }
        }

        /// <summary>
        /// Change Email
        /// </summary>
        [Test]
        public void ST_29()
        {
            var settingsTestData = _testData.Where(t => t.TestDataId.Equals("ST-29"));
            foreach (var testData in settingsTestData)
            {
                PerformChangeEmail(testData);
            }

            if (settingsTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        ///
        /// Change phone number
        /// 
        [Test]
        public void ST_30()
        {
            var settingsTestData = _testData.Where(t => t.TestDataId.Equals("ST-30"));
            foreach (var testData in settingsTestData)
            {
                PerformChangeMsisdn(testData);
            }

            if (settingsTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        private void PerformChangeMsisdn(TestDataSet testData)
        {
            //Open settings
            OpenSettingsViaMenu(testData);

            //start change msisdn interaction
            var settingsInteraction = _appInteraction.SettingsInteraction;
            var msisdnInteraction = settingsInteraction.StartPhoneNumberInteraction();

            //Input new phonenumber
            msisdnInteraction.InputPhoneNumber(testData.SettingsUserInputData.NewMsisdn);
            msisdnInteraction.Next();

            //input new msisdn otp
            msisdnInteraction.InputOtp(testData.SettingsUserInputData.NewMsisdnOtp);
            msisdnInteraction.Next();

            //Did phone-number change?
            settingsInteraction.AssertPhonenumberMatches(testData.SettingsValidationData.DisplayedNewMsisdn);

            //Start change msisdn interaction, so we can change msisdn back
            msisdnInteraction = settingsInteraction.StartPhoneNumberInteraction();

            //input old msisdn
            msisdnInteraction.InputPhoneNumber(testData.LoginInputData.PhoneNumber);
            msisdnInteraction.Next();

            //input otp for old msisdn
            msisdnInteraction.InputOtp(testData.SettingsUserInputData.oldMsisdnOtp);
            msisdnInteraction.Next();

            //Was phonenumber changed back?
            settingsInteraction.AssertPhonenumberMatches(testData.SettingsValidationData.DisplayedOldMsisdn);
        }

        /// <summary>
        /// Change Pin
        /// </summary>
        [Test]
        public void ST_31()
        {
            var settingsTestData = _testData.Where(t => t.TestDataId.Equals("ST-31"));
            foreach (var testData in settingsTestData)
            {
                PerformChangePin(testData);
            }

            if (settingsTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// Delete Agreement
        /// </summary>
        //[Test]
        public void ST_25()
        {
            var testData = _testData.FirstOrDefault();
            PerformDeleteAgreement(testData);
        }

        /// <summary>
        /// Change bank via app
        /// </summary>
        [Test]
        public void ST_24()
        {
            Assert.Inconclusive("No testperson available with two different banks");

            var settingsTestData = _testData.Where(t => t.TestDataId.Equals("ST-24"));
            foreach (var testData in settingsTestData)
            {
                PerformChangeBank(testData);
            }

            if (settingsTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// Soft-block agreement via app
        /// </summary>
        [Test]
        public void ST_22()
        {
            var settingsTestData = _testData.Where(t => t.TestDataId.Equals("ST-22"));
            foreach (var testData in settingsTestData)
            {
                BrowserInteraction browserInteraction = null;
                try
                {
                    browserInteraction = new BrowserInteraction(SetupTeardown.SetupBrowser());

                    TestHelpers.TestHelpers.PerformBlockAgreement(testData, _appInteraction);

                    //Unblock so user is ready again
                    TestHelpers.TestHelpers.PerformUnblockBlockAgreement(testData, _appInteraction, browserInteraction);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    browserInteraction?.CloseAndDisposeBrowser();
                }
            }

            if (settingsTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");            
        }

        /// <summary>
        /// Change Account within the same bank
        /// </summary>
        [Test]
        public void ST_26()
        {
            var settingsTestData = _testData.Where(t => t.TestDataId.Equals("ST-26"));
            foreach (var testData in settingsTestData)
            {
                PerformChangeAccount(testData);
            }

            if (settingsTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// Change daily Limit amount
        /// </summary>
        [Test]
        public void ST_27()
        {
            var settingsTestData = _testData.Where(t => t.TestDataId.Equals("ST-27"));
            foreach (var testData in settingsTestData)
            {
                var mainmenu = _appInteraction.MainMenuInteractionInteraction;
                mainmenu.ShowMenu();
                mainmenu.Settings();

                var settingsInteraction = _appInteraction.SettingsInteraction;
                var currentDailyLimit = Decimal.Parse(settingsInteraction.CurrentDailyLimit);

                TestHelpers.TestHelpers.ChangeDailyLimit("1", _appInteraction); //Change 
                TestHelpers.TestHelpers.ChangeDailyLimit(currentDailyLimit.ToString("N2"), _appInteraction); //Change back

                _appInteraction.PhysicalBackButton(); //Back to mainview
            }

            if (settingsTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }
        
        private void PerformChangeAccount(TestDataSet testData)
        {
            //Open settings
            OpenSettingsViaMenu(testData);
            var settingsInteraction = _appInteraction.SettingsInteraction;

            //Remember current limit
            var oldaccount = settingsInteraction.CurrentAccount;

            //Change to new account
            var newAccount = testData.SettingsUserInputData.BankAccount;
            settingsInteraction.ChangeAccount(newAccount);
            
            //Assert that limit was changed
            settingsInteraction.AssertCurrenAccountMatches(newAccount);
            
            //Change back to old account
            settingsInteraction.ChangeAccount(oldaccount);

            //Assert that limit was changed back
            settingsInteraction.AssertCurrenAccountMatches(oldaccount);

            //Navigate back to MainMenu
            _appInteraction.MainMenuInteractionInteraction.ShowMenu();
            _appInteraction.MainMenuInteractionInteraction.Swipp();
        }
        
        //private void PerformBlockAgreement(TestDataSet testData)
        //{
        //    //Open settings
        //    OpenSettingsViaMenu(testData);
        //    var settingsInteraction = _appInteraction.SettingsInteraction;

        //    settingsInteraction.BlockAgreement(testData.LoginInputData.Pin);
        //    //validate
        //    settingsInteraction.ValidateBlockView("Aftale spærret", "Du kan ophæve spærringen med NemID");
        //}

        private void PerformChangeBank(TestDataSet testData)
        {
            //Open settings
            OpenSettingsViaMenu(testData);
            var settingsInteraction = _appInteraction.SettingsInteraction;
            settingsInteraction.ChangeBank(testData.LoginInputData.Pin);
        }

        private void PerformDeleteAgreement(TestDataSet testData)
        {
            //Open settings
            OpenSettingsViaMenu(testData);
            var settingsInteraction = _appInteraction.SettingsInteraction;

            settingsInteraction.DeleteAgreement(testData.LoginInputData.Pin);
            
        }

        private void PerformChangePin(TestDataSet testData)
        {
            //TODO: Should we do more to assert this worked, than just assume it worked if we get thrown back at the settingsscreen instead of getting an error? 

            //Open settings
            OpenSettingsViaMenu(testData);

            //start change pin interaction
            var settingsInteraction = _appInteraction.SettingsInteraction;
            var pinInteraction = settingsInteraction.StartPinInteraction();

            //Type current pin
            var currentPin = testData.LoginInputData.Pin;
            pinInteraction.InputPin(currentPin);
            //Validate
            pinInteraction.ValidateHeader(testData.SettingsValidationData.InputPinViewHeader);
            pinInteraction.PressNext();

            //Type new pin
            var newPin = testData.SettingsUserInputData.TestPin;
            pinInteraction.InputPin(newPin);
            pinInteraction.ValidateHeader(testData.SettingsValidationData.ChangePinViewHeader);
            pinInteraction.PressNext();

            //Confirm new pin
            pinInteraction.InputPin(newPin);
            pinInteraction.ValidateHeader(testData.SettingsValidationData.ChangePinViewHeader);
            pinInteraction.PressNext();

            //Change pin back
            pinInteraction = settingsInteraction.StartPinInteraction();
            pinInteraction.InputPin(newPin);
            pinInteraction.ValidateHeader(testData.SettingsValidationData.InputPinViewHeader);
            pinInteraction.PressNext();
            pinInteraction.InputPin(currentPin);
            pinInteraction.ValidateHeader(testData.SettingsValidationData.ChangePinViewHeader);
            pinInteraction.PressNext();
            pinInteraction.InputPin(currentPin);
            pinInteraction.ValidateHeader(testData.SettingsValidationData.ChangePinViewHeader);
            pinInteraction.PressNext();

            //navigate back to main
            _appInteraction.MainMenuInteractionInteraction.ShowMenu();
            _appInteraction.MainMenuInteractionInteraction.Swipp();
        }

        private void OpenSettingsViaMenu(TestDataSet testData)
        {
            //Is the menu accessible?
            var menu = _appInteraction.MainMenuInteractionInteraction;
            Assert.AreEqual(menu.IsMenuAccessible, true);

            //Open settings
            menu.ShowMenu();
            menu.Settings();
            _appInteraction.SettingsInteraction.Validate(testData.SettingsValidationData.SettingsViewHeader);
            
        }

        private void PerformChangeNickname(TestDataSet testData)
        {
            //Open settings
            OpenSettingsViaMenu(testData);

            //Remember current nickname
            var settingsInteraction = _appInteraction.SettingsInteraction;
            var oldNickname = settingsInteraction.CurrentNickname;

            //Change to new nickname
            var newNickname = testData.SettingsUserInputData.TestNickname;
            var nicknameInteraction = settingsInteraction.StartNicknameInteraction();
            nicknameInteraction.InputNicknameAndSave(newNickname);

            //Assert that nickname was changed
            settingsInteraction.AssertCurrentNicknameMatches(newNickname);

            //Change back to old nickname
            nicknameInteraction = settingsInteraction.StartNicknameInteraction();
            nicknameInteraction.InputNicknameAndSave(oldNickname);

            //Assert that nickname was changed back
            settingsInteraction.AssertCurrentNicknameMatches(oldNickname);

            //Navigate back to main menu
            _appInteraction.MainMenuInteractionInteraction.ShowMenu();
            _appInteraction.MainMenuInteractionInteraction.Swipp();
        }

        private void PerformChangeEmail(TestDataSet testData)
        {
            //Open settings
            OpenSettingsViaMenu(testData);

            //Remember current email
            var settingsInteraction = _appInteraction.SettingsInteraction;
            var oldEmail = settingsInteraction.CurrentEmail;

            //Change to new email
            var newEmail = testData.SettingsUserInputData.TestMail;
            var emailInteraction = settingsInteraction.StartEmailInteraction();
            emailInteraction.InputEmailAndSave(newEmail);

            //Assert that email was changed
            settingsInteraction.AssertEmailMatches(newEmail);

            //Change back to old email
            emailInteraction = settingsInteraction.StartEmailInteraction();
            emailInteraction.InputEmailAndSave(oldEmail);

            //Assert that nickname was changed back
            settingsInteraction.AssertEmailMatches(oldEmail);

            //Navigate back to MainMenu
            _appInteraction.MainMenuInteractionInteraction.ShowMenu();
            _appInteraction.MainMenuInteractionInteraction.Swipp();
        }
        

        private void PerformTransferDailyLimitExeeded(TestDataSet testData)
        {
            //Start transfer
            _appInteraction.MainViewInteraction.Transfer();

            //Transfer to user (Enter amount -> Choose recipient -> Confirm -> Summary)
            var requestInteraction = _appInteraction.TransferAndRequestInteraction;
            //EnterAmountView
            requestInteraction.EnterAmount(testData.TransactionInputData.Amount);
            //validate Entered Amount
            requestInteraction.EnterAmountScreen.Validate(testData.TransactionValidationData);
            requestInteraction.EnterAmountScreen.ValidateFail();
            //Back to mainview
            requestInteraction.SummaryScreen.CloseCompletedFlow();
        }

        [TearDown]
        public void TearDown()
        {
            SetupTeardown.TearDown();
        }
    }
}

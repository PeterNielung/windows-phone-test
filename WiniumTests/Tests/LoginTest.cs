﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using NUnit.Framework.Internal;
using Swipp.Consumer.Core.Models;
using Swipp.Consumer.Core.Utility;
using WiniumTests.TestData;
using WiniumTests.Utility;

namespace WiniumTests.Tests
{
    [TestFixture]
    public class LoginTest
    {
        private AppInteraction _appInteraction;
        private BrowserInteraction _browserInteraction;
        private List<TestDataSet> _testData;

        #region Setup
        [OneTimeSetUp]
        public void PrepareTestData()
        {
            //Initialize testData
            _testData = TestDataFactory.GetTestDataSets(0);
        }

        [SetUp]
        public void SetupApp()
        {
            _appInteraction = new AppInteraction(SetupTeardown.SetupUpApp());

            _browserInteraction = new BrowserInteraction(SetupTeardown.SetupBrowser());
        }
        

        #endregion
        
        /// <summary>
        /// Enroll new Swipp 2.0 user
        /// We do this by deleting the old user, signing up a new. Deleting that user and signing up the old
        /// </summary>
        [Test]
        public void ST_20()
        {

            var loginTestData = _testData.Where(t => t.TestDataId.Equals("ST-20"));

            foreach (var testData in loginTestData)
            {
                //TestHelpers.TestHelpers.Login(testData, _appInteraction);
                //TestHelpers.TestHelpers.DeleteAgreement(testData.LoginInputData.Pin, _appInteraction);
                //SignupUser(testData, true); // ST-20   
                //ValidateUser(testData, true);
                TestHelpers.TestHelpers.DeleteAgreement(testData.NewLoginInputData.Pin, _appInteraction);
                SignupUser(testData, false);
                ValidateUser(testData, false);
            }

            if (loginTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        private void ValidateUser(TestDataSet testData, bool useNewData)
        {
            _appInteraction.MainMenuInteractionInteraction.ShowMenu();
            _appInteraction.MainMenuInteractionInteraction.Settings();
            var settingsInteraction = _appInteraction.SettingsInteraction;
            var phoneNumber = useNewData ? new MsisdnModel("45", testData.NewLoginInputData.PhoneNumber) : new MsisdnModel("45", testData.LoginInputData.PhoneNumber);
            var email = useNewData ? testData.NewLoginInputData.Email : testData.LoginInputData.Email;
            var nickname = useNewData ? testData.NewLoginInputData.Nickname : testData.LoginInputData.Nickname;
            var bankname = useNewData ? testData.NewLoginInputData.BankName : testData.LoginInputData.BankName;

            Assert.AreEqual(phoneNumber.DisplayString, settingsInteraction.CurrentPhonenumber);
            Assert.AreEqual(email, settingsInteraction.CurrentEmail);
            Assert.AreEqual(nickname, settingsInteraction.CurrentNickname);
            Assert.AreEqual(bankname, settingsInteraction.CurrentBankName);
            
            _appInteraction.PhysicalBackButton();
        }

        private void SignupUser(TestDataSet testData, bool useNewData)
        {
            var phoneNumber = useNewData ? testData.NewLoginInputData.PhoneNumber : testData.LoginInputData.PhoneNumber;
            var cpr = useNewData ? testData.NewLoginInputData.Cpr : testData.LoginInputData.Cpr;
            var otp = useNewData ? testData.NewLoginInputData.Otp : testData.LoginInputData.Otp;
            var email = useNewData ? testData.NewLoginInputData.Email : testData.LoginInputData.Email;
            var nickname = useNewData ? testData.NewLoginInputData.Nickname : testData.LoginInputData.Nickname;
            var bankname = useNewData ? testData.NewLoginInputData.BankName : testData.LoginInputData.BankName;
            var bankaccount = useNewData ? testData.NewLoginInputData.BankAccount : testData.LoginInputData.BankAccount;
            var pin = useNewData ? testData.NewLoginInputData.Pin : testData.LoginInputData.Pin;
            var cprPassword = useNewData ? testData.NewLoginInputData.CprPassword : testData.LoginInputData.CprPassword;

            var enrollment = _appInteraction.EnrollmentInteraction;

            //Press start at Introview
            enrollment.PressIntroStartButton();

            //Enter Msisdn
            enrollment.EnterMsisdn(phoneNumber);
            enrollment.PressKeypadNextButton();

            //Enter Otp
            enrollment.WaitForHeader("Indtast aktiveringskode");
            enrollment.EnterOtp(otp);
            enrollment.PressKeypadNextButton();
            SpinWait.SpinUntil(() => false, 4000);

            //Fetch NemIdUrl
            var url = enrollment.NemidScreen.GetNemidUrlFromWebView();

            //Handle Nemid in browser
            //navigate to Nemid
            _browserInteraction.NavigateTo(url);

            //input UserId end Password
            _browserInteraction.InputUseridAndPassword(cpr, cprPassword);
            _browserInteraction.PressNext();

            //GetNemidKey
            var inputKey = _browserInteraction.GetInputKeyFromCard();

            //Enter key
            _browserInteraction.EnterNemidKey(inputKey);

            //ClickSubmit
            _browserInteraction.ClickSubmit();

            //subtract redirectUrl
            var redirectUrl = _browserInteraction.GetCurrentUrl();
            
            //redirects Webview
            enrollment.NemidScreen.RedirectAppNemidView(redirectUrl);

            //Cpr:
            enrollment.EnterCpr(cpr);
            enrollment.PressKeypadNextButton();

            //ChooseBank
            enrollment.EnterBankName(bankname);
            enrollment.ChooseBank(bankname);

            //ChooseAccount
            enrollment.ChooseAccount(bankaccount);

            //EnterNickname
            enrollment.EnterNickname(nickname);
            enrollment.ConfirmNickname();

            //EnterEmail
            enrollment.EnterEmail(email);
            enrollment.ConfirmEmail();

            //ChoosePin
            enrollment.EnterPin(pin);
            enrollment.WaitForHeader("Indtast PIN-kode");
            enrollment.ConfirmPin(pin);

            //ConfirmAgreement
            enrollment.ConfirmTerms();

            SpinWait.SpinUntil(() => false, 4000);
        }


        /// <summary>
        /// Unblock Soft-block agreement via app
        /// </summary>
        [Test]
        public void ST_23()
        {
            var loginTestData = _testData.Where(t => t.TestDataId.Equals("ST-23"));

            foreach (var testData in loginTestData)
            {
                TestHelpers.TestHelpers.Login(testData, _appInteraction);
                TestHelpers.TestHelpers.PerformBlockAgreement(testData, _appInteraction);
                TestHelpers.TestHelpers.PerformUnblockBlockAgreement(testData, _appInteraction, _browserInteraction);
            }

            if (loginTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }
        
        [TearDown]
        public void TearDown()
        {
            _browserInteraction.CloseAndDisposeBrowser();
            SetupTeardown.TearDown();
        }

        
        /*
        [Test]
        public void LockedAgreementEnrollmentTest()
        {
            //Press start at Introview
            _appInteraction.EnrollmentInteraction.PressIntroStartButton();

            //Enter Msisdn
            _appInteraction.EnrollmentInteraction.EnterMsisdn("90909090");
            _appInteraction.EnrollmentInteraction.PressKeypadNextButton();
            SpinWait.SpinUntil(() => false, 7000);

            //Enter Otp
            _appInteraction.EnrollmentInteraction.EnterOtp("2319");
            _appInteraction.EnrollmentInteraction.PressKeypadNextButton();
            SpinWait.SpinUntil(() => false, 4000);

            //Fetch NemIdUrl
            var url = _appInteraction.EnrollmentInteraction.NemidScreen.GetNemidUrlFromWebView();


            //Handle Nemid in browser
            //navigate to Nemid
            _browserInteraction.NavigateTo(url);

            //input UserId end Password
            _browserInteraction.InputUseridAndPassword("1711662762", "asasas12");
            _browserInteraction.PressNext();

            var redirectUrl = _browserInteraction.GetCurrentUrl();

            _browserInteraction.CloseAndDisposeBrowser();

            _appInteraction.EnrollmentInteraction.NemidScreen.RedirectAppNemidView(redirectUrl);

            //validate
            _appInteraction.EnrollmentInteraction.ValidateError("AUTH007", "Fejl");
        }
        */
    }
}

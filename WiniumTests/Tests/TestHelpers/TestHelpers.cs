﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using WiniumTests.TestData;
using WiniumTests.Utility;

namespace WiniumTests.Tests.TestHelpers
{
    /// <summary>
    /// Contains common functionality shared between the tests.
    /// </summary>
    class TestHelpers
    {

        public static void Login(TestDataSet testdata, AppInteraction appInteraction)
        {
            var credentials = testdata.LoginInputData;

            //Assumes we are at the loginscreen
            var loginInteraction = appInteraction.LoginInteraction;
            loginInteraction.Login(credentials.Pin);
        }

        public static void Logout(AppInteraction appInteraction)
        {
            var mainmenuInteraction = appInteraction.MainMenuInteractionInteraction;
            mainmenuInteraction.ShowMenu();
            mainmenuInteraction.Logout();
        }

        public static void DeleteAgreement(string pin, AppInteraction appInteraction)
        {
            //Delete agreement
            var menu = appInteraction.MainMenuInteractionInteraction;
            menu.ShowMenu();
            menu.Settings();
            appInteraction.SettingsInteraction.DeleteAgreement(pin);
        }

        //Assumes we are at the long signup/login
        public static void PerformUnblockBlockAgreement(TestDataSet testData, AppInteraction appInteraction, BrowserInteraction browserInteraction)
        {
            var enrollment = appInteraction.EnrollmentInteraction;

            //Assume that the agreement has just been blocked, so we should be at the following screen:
            enrollment.WaitForHeader("Aftale spærret");

            //Click to get to nemId
            enrollment.BlockedViewPressNextButton();

            //Handle Nemid in browser
            //navigate to Nemid
            var url = enrollment.NemidScreen.GetNemidUrlFromWebView();
            browserInteraction.NavigateTo(url);

            //input UserId end Password
            browserInteraction.InputUseridAndPassword(testData.LoginInputData.Cpr, testData.LoginInputData.CprPassword);
            browserInteraction.PressNext();

            //GetNemidKey
            var inputKey = browserInteraction.GetInputKeyFromCard();

            //Enter key
            browserInteraction.EnterNemidKey(inputKey);

            //ClickSubmit
            browserInteraction.ClickSubmit();

            //subtract redirectUrl
            var redirectUrl = browserInteraction.GetCurrentUrl();
            
            //redirects Webview
            appInteraction.EnrollmentInteraction.NemidScreen.RedirectAppNemidView(redirectUrl);

            //We should be at the screen, where we input the new pin now.
            enrollment.WaitForHeader("Skift PIN-kode");
            enrollment.EnterPin(testData.LoginInputData.Pin);
            enrollment.Wait(5000); //The next screen has the same header, so we cannot wait for a new header, thus we wait for a while instead.
            enrollment.EnterPin(testData.LoginInputData.Pin);

            //validate
            appInteraction.MainViewInteraction.Validate("Overfør", "Betal", "Anmod");
        }

        /// <summary>
        /// Assumes we are at the mainview
        /// </summary>
        /// <param name="testData"></param>
        /// <param name="appInteraction"></param>
        public static void PerformBlockAgreement(TestDataSet testData, AppInteraction appInteraction)
        {
            //Open settings
            var menu = appInteraction.MainMenuInteractionInteraction;
            menu.ShowMenu();
            menu.Settings();

            //Block agreement
            var settings = appInteraction.SettingsInteraction;
            settings.BlockAgreement(testData.LoginInputData.Pin);
        }

        /// <summary>
        /// Assumes we are at the mainview
        /// </summary>
        /// <param name="testData"></param>
        /// <param name="appInteraction"></param>
        /// <param name="sessionExpired"></param>
        public static void PerformTransferUptoDailyLimit(TestDataSet testData, AppInteraction appInteraction, bool sessionExpired = false)
        {
            //First find out what the remaining daily limit is. We can get this info on the enter amount screen
            //If we enter an amount higher than the currently allowed.
            appInteraction.MainViewInteraction.Transfer();
            var transferInteraction = appInteraction.TransferAndRequestInteraction;
            transferInteraction.EnterAmount("999999");

            //This should have created an error-mmessage... If not, we crash here as we should
            var dailyLimitRemainingText = appInteraction.TransferAndRequestInteraction.EnterAmountScreen.EnterAmountDailyLimitErrorMessage;

            //Extract remaining amount from error-message
            var dailylimitRemainingRegex = new Regex(@"[0-9|.]*,*[0-9]+");
            var dailylimitRemaining = Decimal.Parse(dailylimitRemainingRegex.Match(dailyLimitRemainingText).Value);

            //Goto mainview again and then to settings to change daily limit
            appInteraction.PhysicalBackButton();
            var mainmenu = appInteraction.MainMenuInteractionInteraction;
            mainmenu.ShowMenu();
            mainmenu.Settings();

            //read what current daily limit is set at
            var settingsInteraction = appInteraction.SettingsInteraction;
            var oldDailyLimit = Decimal.Parse(settingsInteraction.CurrentDailyLimit);

            //What should new daily limit be? It should be what we have transfered so far + 1 kr
            var transferredSoFar = oldDailyLimit - dailylimitRemaining;
            var newDailyLimit = (transferredSoFar + 1).ToString();

            ChangeDailyLimit(newDailyLimit, appInteraction);

            //Go to transfer and transfer transferredSoFar + 1 kr
            mainmenu.ShowMenu();
            mainmenu.Swipp();
            testData.TransactionInputData.Amount = "1,00";
            testData.TransactionValidationData.DisplayAmount = "1,00"; //"1 kr.";
            PerformTransfer(testData, appInteraction, sessionExpired);

            //Goto settings and change dailylimit back
            mainmenu.ShowMenu();
            mainmenu.Settings();

            ChangeDailyLimit(oldDailyLimit.ToString(), appInteraction);
        }

        public static void ChangeDailyLimit(string newDailyLimit, AppInteraction appInteraction)
        {
            var settingsInteraction = appInteraction.SettingsInteraction;

            //Strip "." from the new limit, as that is just for visuals in Denmark
            var newDailyLimitWithoutDots = newDailyLimit.Replace(".", "");

            //Change daily limit to new value
            var dailyLimitInteraction = settingsInteraction.StartDailyLimitInteraction();
            dailyLimitInteraction.InputAmountLimitAndSave(newDailyLimitWithoutDots);

            //Verify daily limit has changed
            var newDisplayedLimit = settingsInteraction.CurrentDailyLimit.Replace(".", "");
            var newDisplayedLimitExpected = newDailyLimitWithoutDots.Contains(",") ? newDailyLimitWithoutDots : newDailyLimitWithoutDots + ",00";
            Assert.AreEqual(newDisplayedLimit, newDisplayedLimitExpected);
        }

        /// <summary>
        /// Assumes we are at the mainview
        /// </summary>
        /// <param name="testData"></param>
        /// <param name="appInteraction"></param>
        /// <param name="sessionExpired"></param>
        public static void PerformTransfer(TestDataSet testData, AppInteraction appInteraction, bool sessionExpired = false)
        {
            //Start transfer
            appInteraction.MainViewInteraction.Transfer();

            //Transfer to user (Enter amount -> Choose recipient -> Confirm -> Summary)
            var requestInteraction = appInteraction.TransferAndRequestInteraction;
            //EnterAmountView
            requestInteraction.EnterAmount(testData.TransactionInputData.Amount);
            //validate Entered Amount
            requestInteraction.EnterAmountScreen.Validate(testData.TransactionValidationData);
            requestInteraction.ConfirmEnterAmount();

            //ChooseRecipientView
            requestInteraction.EnterRecipient(testData.TransactionInputData.RecipientPhoneNumber);
            //validate Recipient
            requestInteraction.ChooseRecipientScreen.Validate(testData.TransactionValidationData);
            requestInteraction.ConfirmEnterRecipient(testData.TransactionInputData.RecipientPhoneNumber);

            //ConfirmationView
            requestInteraction.EnterTransferOrRequestNote(testData.TransactionInputData.Note);
            requestInteraction.ConfirmationScreen.Validate(testData.TransactionValidationData);
            requestInteraction.ConfirmTransferOrRequest();

            //PinView if session is expired
            if (sessionExpired)
            {
                Login(testData, appInteraction);
                SpinWait.SpinUntil(() => false, 2000);
                requestInteraction.ConfirmTransferOrRequest();
            }

            //SummaryView Validate values
            requestInteraction.SummaryScreen.Validate(testData.TransactionValidationData);

            //Back to mainview
            requestInteraction.SummaryScreen.CloseCompletedFlow();

        }
    }
}

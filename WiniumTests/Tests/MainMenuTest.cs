﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium.Remote;
using WiniumTests.TestData;
using WiniumTests.Utility;

namespace WiniumTests.Tests
{
    /// <summary>    
    /// Tests that after a login the user can change between all menu points.
    /// And that back returns the user to the main screen.
    /// </summary>
    [TestFixture]
    public class MainMenuTest
    {
        private AppInteraction _appInteraction;
        private List<TestDataSet> _testData;

        private const string SettingsHeader = "Indstillinger";
        private const string HistoryHeader = "Aktiviteter";
        private const string HelpHeader = "";

        #region SetupAndLogin
        [OneTimeSetUp]
        public void Setup()
        {
            _appInteraction = new AppInteraction(SetupTeardown.SetupUpApp());

            //Initialize testData
            _testData = TestDataFactory.GetTestDataSets(0);
        }
        

        #endregion


        //[Test]
        public void MainScreenToSettings()
        {
            var mainmenuInteraction = _appInteraction.MainMenuInteractionInteraction;
            mainmenuInteraction.ShowMenu();
            mainmenuInteraction.Settings();
            //validate
            mainmenuInteraction.ValidateSettingsView(SettingsHeader);
        }

        /// <summary>
        /// View Activities list
        /// </summary>
        [Test]
        public void ST_21()
        {
            var mainMenuTestData = _testData.Where(t => t.TestDataId.Equals("ST-21"));

            foreach (var testData in mainMenuTestData)
            {
                //Login
                TestHelpers.TestHelpers.Login(testData, _appInteraction);

                //View activities
                var mainmenu = _appInteraction.MainMenuInteractionInteraction;
                mainmenu.ShowMenu();
                mainmenu.History();
                
                //At mainmenu?
                _appInteraction.HistoryInteraction.Validate("Aktiviteter");

                //Logout again
                mainmenu.ShowMenu();
                mainmenu.Logout();
            }
            if (mainMenuTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
            
        }

        //[Test]
        public void MainScreenToHelp()
        {
            var mainmenuInteraction = _appInteraction.MainMenuInteractionInteraction;
            mainmenuInteraction.ShowMenu();
            mainmenuInteraction.Help();
            //Assert.AreEqual(string.Empty, _appInteraction.UiMap.Swipp_App_Window.TitleTextBox.DisplayText);
        }

        //[Test]
        public void MainScreenToTerms()
        {
            var mainmenuInteraction = _appInteraction.MainMenuInteractionInteraction;
            mainmenuInteraction.ShowMenu();
            mainmenuInteraction.Terms();            
        }

        //[Test]
        public void MenuLogout()
        {
            _appInteraction.MainMenuInteractionInteraction.ShowMenu();
            _appInteraction.MainMenuInteractionInteraction.Logout();
        }

        [OneTimeTearDown]
        public void ClassCleanup()
        {
            SetupTeardown.TearDown();
        }
    }
}

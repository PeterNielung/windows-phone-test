﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using WiniumTests.Interactions;
using WiniumTests.TestData;
using WiniumTests.Utility;

namespace WiniumTests.Tests.TransactionTests
{
    [TestFixture]
    public class TransferTest
    {
        private AppInteraction _appInteraction;
        private List<TestDataSet> _testData;

        #region SetupAndLogin
        [OneTimeSetUp]
        public void PrepareTestData()
        {
            //Initialize testData
            _testData = TestDataFactory.GetTestDataSets(0);
        }

        [SetUp]
        public void SetupApp()
        {
            _appInteraction = new AppInteraction(SetupTeardown.SetupUpApp());
            Login();
        }

        private void Login()
        {
            var credentials = _testData.FirstOrDefault().LoginInputData;

            //Assumes we are at the loginscreen
            var loginInteraction = _appInteraction.LoginInteraction;
            loginInteraction.Login(credentials.Pin);
        }

        //Used during a test
        private void Login(LoginInputData loginInput)
        {
            //Assumes we are at the loginscreen
            var loginInteraction = _appInteraction.LoginInteraction;
            loginInteraction.Login(loginInput.Pin);
        }

#endregion

        /// <summary>
        /// Transfer to Swipp 1.0 user (Same DC)
        /// </summary>
        [Test]
        public void ST_5()
        {
            var numbersOfTimesRun = 0;
            var transferTestData = _testData.Where(t => t.TestDataId.Equals("ST-5"));

            foreach (var testData in transferTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    TestHelpers.TestHelpers.PerformTransfer(testData, _appInteraction, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    TestHelpers.TestHelpers.PerformTransfer(testData, _appInteraction);
                    numbersOfTimesRun++;
                }
            }

            if (transferTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// Transfer to Swipp 1.0 user (diferent DC)
        /// </summary>
        [Test]
        public void ST_6()
        {
            var numbersOfTimesRun = 0;
            var transferTestData = _testData.Where(t => t.TestDataId.Equals("ST-6"));

            foreach (var testData in transferTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    TestHelpers.TestHelpers.PerformTransfer(testData, _appInteraction, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    TestHelpers.TestHelpers.PerformTransfer(testData, _appInteraction);
                    numbersOfTimesRun++;
                }
            }

            if (transferTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// Transfer to Swipp 2.0 user (same DC)
        /// </summary>
        [Test]
        public void ST_7()
        {
            var numbersOfTimesRun = 0;
            var transferTestData = _testData.Where(t => t.TestDataId.Equals("ST-7"));

            foreach (var testData in transferTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    TestHelpers.TestHelpers.PerformTransfer(testData, _appInteraction, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    TestHelpers.TestHelpers.PerformTransfer(testData, _appInteraction);
                    numbersOfTimesRun++;
                }
            }

            if (transferTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        [Test]
        public void ExceptionTest()
        {
            var numbersOfTimesRun = 0;
            var transferTestData = _testData.Where(t => t.TestDataId.Equals("ST-7"));


            foreach (var testData in transferTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    TestHelpers.TestHelpers.PerformTransfer(testData, _appInteraction, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    TestHelpers.TestHelpers.PerformTransfer(testData, _appInteraction);
                    numbersOfTimesRun++;
                }
            }

            if (transferTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// Transfer to Swipp 2.0 user (different DC)
        /// </summary>
        [Test]
        public void ST_8()
        {
            var numbersOfTimesRun = 0;
            var transferTestData = _testData.Where(t => t.TestDataId.Equals("ST-8"));

            foreach (var testData in transferTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    TestHelpers.TestHelpers.PerformTransfer(testData, _appInteraction, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    TestHelpers.TestHelpers.PerformTransfer(testData, _appInteraction);
                    numbersOfTimesRun++;
                }
            }

            if (transferTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// remaining daily limit amount. (DailyLimit matched)
        /// </summary>
        [Test]
        public void ST_243_1()
        {
            var numbersOfTimesRun = 0;
            var transferTestData = _testData.Where(t => t.TestDataId.Equals("ST-243-1"));

            foreach (var testData in transferTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    TestHelpers.TestHelpers.PerformTransferUptoDailyLimit(testData, _appInteraction, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    TestHelpers.TestHelpers.PerformTransferUptoDailyLimit(testData, _appInteraction);
                    numbersOfTimesRun++;
                }
                
            }

            if (transferTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }
        
        /// <summary>
        /// remaining daily limit amount. (DailyLimit exeeded)
        /// </summary>
        [Test]
        public void ST_243_2()
        {
            var transferTestData = _testData.Where(t => t.TestDataId.Equals("ST-243-2"));

            foreach (var testData in transferTestData)
            {
                PerformTransferDailyLimitExeeded(testData);
            }

            if (transferTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        
        private void PerformTransferDailyLimitExeeded(TestDataSet testData)
        {
            //Start transfer
            _appInteraction.MainViewInteraction.Transfer();

            //Transfer to user (Enter amount -> Choose recipient -> Confirm -> Summary)
            var requestInteraction = _appInteraction.TransferAndRequestInteraction;
            //EnterAmountView
            requestInteraction.EnterAmount(testData.TransactionInputData.Amount);
            //validate Entered Amount
            requestInteraction.EnterAmountScreen.Validate(testData.TransactionValidationData);
            requestInteraction.EnterAmountScreen.ValidateFail();

            //Since the amount is too high, we should not be able to push next, and should thus still be at the same screen after trying to do so.
            requestInteraction.ConfirmEnterAmount();
            requestInteraction.EnterAmountScreen.Validate(testData.TransactionValidationData);

            //Back to mainview
            requestInteraction.SummaryScreen.CloseCompletedFlow();

        }

        [TearDown]
        public void TearDown()
        {
            SetupTeardown.TearDown();
        }
    }
}

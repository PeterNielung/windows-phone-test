﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using NUnit.Framework.Internal;
using Swipp.Consumer.Core.TestLibrary;
using WiniumTests.TestData;
using WiniumTests.Utility;

namespace WiniumTests.Tests.TransactionTests
{
    [TestFixture]
    public class RequestTest
    {
        private AppInteraction _appInteraction;
        private List<TestDataSet> _testData;

        #region SetupAndLogin
        [OneTimeSetUp]
        public void PrepareTestData()
        {
            //Initialize testData
            _testData = TestDataFactory.GetTestDataSets(0);
        }

        [SetUp]
        public void SetupApp()
        {
            _appInteraction = new AppInteraction(SetupTeardown.SetupUpApp());
            
            Login();
        }

        private void Login()
        {
            var credentials = _testData.FirstOrDefault().LoginInputData;

            //Assumes we are at the loginscreen
            var loginInteraction = _appInteraction.LoginInteraction;
            loginInteraction.Login(credentials.Pin);
        }

        //Used during a test
        private void Login(LoginInputData loginInput)
        {
            //Assumes we are at the loginscreen
            var loginInteraction = _appInteraction.LoginInteraction;
            loginInteraction.Login(loginInput.Pin);
        }

        #endregion

        /// <summary>
        /// Request money from swipp 1.0 user
        /// </summary>
        [Test]
        public void ST_15()
        {
            var numbersOfTimesRun = 0;
            var requestTestData = _testData.Where(t => t.TestDataId.Equals("ST-15"));

            foreach (var testData in requestTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    PerformRequestFail(testData);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    PerformRequestFail(testData);
                    numbersOfTimesRun++;
                }
            }

            if (requestTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// Request money from swipp 1.0 user
        /// </summary>
        [Test]
        public void ST_16()
        {
            var numbersOfTimesRun = 0;
            var requestTestData = _testData.Where(t => t.TestDataId.Equals("ST-16"));

            foreach (var testData in requestTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    PerformRequest(testData, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    PerformRequest(testData);
                    numbersOfTimesRun++;
                }
            }

            if (requestTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// Send Request to 2p
        /// </summary>
        [Test]
        public void ST_241()
        {
            var numbersOfTimesRun = 0;
            var requestTestData = _testData.Where(t => t.TestDataId.Equals("ST-241"));

            foreach (var testData in requestTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    PerformRequest(testData, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    PerformRequest(testData);
                    numbersOfTimesRun++;
                }
            }

            if (requestTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        ///  request money from small merchant 1e (Organisation)
        /// </summary>
        [Test]
        public void ST_17()
        {
            var requestTestData = _testData.Where(t => t.TestDataId.Equals("ST-17"));

            foreach (var testData in requestTestData)
            {
                PerformRequestFail(testData);
            }

            if (requestTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        ///  request money from small merchant 1e (Small Merchant)
        /// </summary>
        [Test]
        public void ST_18()
        {
            var requestTestData = _testData.Where(t => t.TestDataId.Equals("ST-18"));

            foreach (var testData in requestTestData)
            {
                PerformRequestFail(testData);
            }

            if (requestTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }
        
        public void CancelRequest()
        {
            foreach (var data in _testData)
            {
                //First perform request
                var transactionId = PerformRequest(data);
                data.TransactionInputData.TransactionId = transactionId;
                data.TransactionValidationData.TransactionId = transactionId;

                //Cancel Request
                PerformCancelRequest(data);
            }
        }

        /// <summary>
        /// Confirm request from 2p
        /// </summary>
        [Test]
        public void ST_242()
        {
            var requestTestData = _testData.Where(t => t.TestDataId.Equals("ST-242"));

            foreach (var testData in requestTestData)
            {
                var requestUser = new Consumer(testData.ConsumerApiData.ConsumerApiPin,
                                            testData.ConsumerApiData.ConsumerApiOtp,
                                            testData.ConsumerApiData.ConsumerApiCountryCode,
                                            testData.ConsumerApiData.ConsumerApiMsisdn,
                                            testData.ConsumerApiData.AppVersion);

                var amount = long.Parse(testData.TransactionInputData.Amount.Replace(",", "").Replace(".", "")); //Strip commaseperator as consumer API does not support that.
                var transactionId = requestUser.MakeRequest("45", testData.LoginInputData.PhoneNumber, amount, testData.TransactionInputData.Note).Result;
                PerformAcceptRequest(transactionId, testData);
            }
            
            if (requestTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        private string PerformRequest(TestDataSet testData, bool sessionExpired = false)
        {
            //Start Request
            _appInteraction.MainViewInteraction.Request();

            //Request to user (Enter amount -> Choose recipient -> Confirm -> Summary)
            var requestInteraction = _appInteraction.TransferAndRequestInteraction;

            //EnterAmountView
            requestInteraction.EnterAmount(testData.TransactionInputData.Amount);

            //validate Entered Amount
            requestInteraction.EnterAmountScreen.Validate(testData.TransactionValidationData);
            requestInteraction.ConfirmEnterAmount();

            //ChooseRecipientView
            requestInteraction.EnterRecipient(testData.TransactionInputData.RecipientPhoneNumber);

            //validate Recipient
            requestInteraction.ChooseRecipientScreen.Validate(testData.TransactionValidationData);
            requestInteraction.ConfirmEnterRecipient(testData.TransactionInputData.RecipientPhoneNumber);

            //ConfirmationView
            requestInteraction.EnterTransferOrRequestNote(testData.TransactionInputData.Note);

            //Validate entered values
            requestInteraction.ConfirmationScreen.Validate(testData.TransactionValidationData);
            requestInteraction.ConfirmTransferOrRequest();
            if (sessionExpired)
            {
                Login(testData.LoginInputData);
                SpinWait.SpinUntil(() => false, 2000);
                requestInteraction.ConfirmTransferOrRequest();
            }
            
            //SummaryView Validate values
            requestInteraction.SummaryScreen.Validate(testData.TransactionValidationData);

            var transactionId = requestInteraction.SummaryScreen.GetTransactionId();

            //Back to mainview
            requestInteraction.SummaryScreen.CloseCompletedFlow();

            return transactionId;
        }

        private void PerformRequestFail(TestDataSet testData)
        {
            //Start Request
            _appInteraction.MainViewInteraction.Request();

            //Request to user (Enter amount -> Choose recipient -> Confirm -> Summary)
            var requestInteraction = _appInteraction.TransferAndRequestInteraction;

            //EnterAmountView
            requestInteraction.EnterAmount(testData.TransactionInputData.Amount);

            //validate Entered Amount
            requestInteraction.EnterAmountScreen.Validate(testData.TransactionValidationData);
            requestInteraction.ConfirmEnterAmount();

            //ChooseRecipientView
            requestInteraction.EnterRecipient(testData.TransactionInputData.RecipientPhoneNumber);

            //validate Recipient
            requestInteraction.ChooseRecipientScreen.Validate(testData.TransactionValidationData);
            requestInteraction.ConfirmEnterRecipient(testData.TransactionInputData.RecipientPhoneNumber);

            //Validate ErrorView
            requestInteraction.ErrorMessageView.Validate(testData.TransactionValidationData);

            //Close
            requestInteraction.SummaryScreen.CloseCompletedFlow();
            requestInteraction.SummaryScreen.CloseCompletedFlow();
        }

        /// <summary>
        /// Expects that we are at the mainview. Accepts the chosen request
        /// </summary>
        private void PerformAcceptRequest(string transactionId, TestDataSet testData)
        {
            //OpenMainMenu
            _appInteraction.MainMenuInteractionInteraction.ShowMenu();

            //Press Activities
            _appInteraction.MainMenuInteractionInteraction.History();

            //find and cancel request(Find transaction -> press cancel -> summary)
            var historyInteraction = _appInteraction.HistoryInteraction;
            historyInteraction.ClickRowWithTransactionId(transactionId);

            var requestInteraction = _appInteraction.TransferAndRequestInteraction;

            requestInteraction.ConfirmationScreen.Accept();

            requestInteraction.SummaryScreen.Validate(testData.TransactionValidationData);
            requestInteraction.SummaryScreen.CloseCompletedFlow();
        }

        /// <summary>
        /// Expects that we are at the mainview. Cancels the top request from the history-/activity view and goes back to the mainview.
        /// </summary>
        private void PerformCancelRequest(TestDataSet testDataSet)
        {
            //OpenMainMenu
            _appInteraction.MainMenuInteractionInteraction.ShowMenu();

            //Press Activities
            _appInteraction.MainMenuInteractionInteraction.History();

            //find and cancel request(Find transaction -> press cancel -> summary)
            var historyInteraction = _appInteraction.HistoryInteraction;
            historyInteraction.ClickRowWithTransactionId(testDataSet.TransactionInputData.TransactionId);

            var requestInteraction = _appInteraction.TransferAndRequestInteraction;

            requestInteraction.SummaryScreen.CancelRequest();
            requestInteraction.SummaryScreen.PressPopupYes();
            requestInteraction.SummaryScreen.CloseCompletedFlow();
            
            //Gets back to mainView
            _appInteraction.MainMenuInteractionInteraction.ShowMenu();
            _appInteraction.MainMenuInteractionInteraction.Swipp();
        }

        [TearDown]
        public void TearDown()
        {
            SetupTeardown.TearDown();
        }
    }
}

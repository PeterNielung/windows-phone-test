﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium.Remote;
using Swipp.Consumer.Core.TestLibrary;
using WiniumTests.TestData;
using WiniumTests.Utility;

namespace WiniumTests.Tests.TransactionTests
{
    [TestFixture]
    public class PaymentTest
    {

        private AppInteraction _appInteraction;
        private List<TestDataSet> _testData;

        [OneTimeSetUp]
        public void PrepareTestData()
        {
            //Initialize testData
            _testData = TestDataFactory.GetTestDataSets(0);
        }

        [SetUp]
        public void SetupApp()
        {
            _appInteraction = new AppInteraction(SetupTeardown.SetupUpApp());
            
            Login();
        }

        private void Login()
        {
            var credentials = _testData.FirstOrDefault().LoginInputData;

            //Assumes we are at the loginscreen
            var loginInteraction = _appInteraction.LoginInteraction;
            loginInteraction.Login(credentials.Pin);
        }

        public void CancelRequestFromMerchantViaPaymentView(string merchantPublic, string merchantPrivate, long amount, string msisdn)
        {
            foreach (var testData in _testData)
            {
                //Goto Paymentview, as the phone will poll for requests while on this view.
                var mainview = _appInteraction.MainViewInteraction;
                mainview.Payment();
                mainview.Wait(1000); //Allow next view to load, before sending request

                //Make request from merchant backend
                var merchant = new Merchant(merchantPublic, merchantPrivate);
                var transactionId = merchant.CreateRequestToNumber(amount, msisdn).Result;

                //We should automatically be sent to the "Confirm payment screen", so cancel request now
                var paymentview = _appInteraction.PaymentInteraction;
                paymentview.Wait(6000); //Allow notification to disappear, before trying to push cancel button
                paymentview.CancelPaymentAtConfirmationScreen();

                //Verify that the values at the summary screen matches
                //TODO
                throw new NotImplementedException();

                //Go back to mainview.
                //TODO
            }
        }

        /// <summary>
        /// Transfer to Small merchant (1E) (Same DC)
        /// </summary>
        [Test]
        public void ST_9()
        {
            var numbersOfTimesRun = 0;
            var paymentTestData = _testData.Where(t => t.TestDataId.Equals("ST-9"));
            foreach (var testData in paymentTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    PerformPayment(testData, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    PerformPayment(testData);
                    numbersOfTimesRun++;
                }
            }

            if (paymentTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }
        
        /// <summary>
        /// Transfer to Small merchant (1E) (Different DC)
        /// </summary>
        [Test]
        public void ST_10()
        {
            var numbersOfTimesRun = 0;
            var paymentTestData = _testData.Where(t => t.TestDataId.Equals("ST-10"));
            foreach (var testData in paymentTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    PerformPayment(testData, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    PerformPayment(testData);
                    numbersOfTimesRun++;
                }
            }

            if (paymentTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// Payments to Large Merchant (2c) via mobil nr, standdard
        /// </summary>
        [Test]
        public void ST_11()
        {
            var numbersOfTimesRun = 0;
            var paymentTestData = _testData.Where(t => t.TestDataId.Equals("ST-11"));
            foreach (var testData in paymentTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    PerformPayment(testData, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    PerformPayment(testData);
                    numbersOfTimesRun++;
                }
            }

            if (paymentTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// Payments to Large Merchant (2c) via mobil nr, standdard
        /// </summary>
        [Test]
        public void ST_12()
        {
            var numbersOfTimesRun = 0;
            var paymentTestData = _testData.Where(t => t.TestDataId.Equals("ST-12"));
            foreach (var testData in paymentTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    PerformPayment(testData, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    PerformPayment(testData);
                    numbersOfTimesRun++;
                }
            }

            if (paymentTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// ST-13: Payment from Swipp 2.0 to Large Merchant (2C) from the same DC via QR-Code
        /// </summary>
        [Test]
        public void ST_13()
        {
            var paymentTestData = _testData.Where(t => t.TestDataId.Equals("ST-13"));

            foreach (var testData in paymentTestData)
            {
                var amount = ConvertAmountStringToLong(testData.TransactionInputData.Amount);
                
                TokenTransactionTest(testData.MerchantData.PublicKey, testData.MerchantData.SecretKey, amount, testData.TransactionValidationData);
            }

            if (paymentTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// ST-14: Payment from Swipp 2.0 to Large Merchant (2C) across different DCs via QR-Code
        /// </summary>
        [Test]
        public void ST_14()
        {
            var paymentTestData = _testData.Where(t => t.TestDataId.Equals("ST-14"));

            foreach (var testData in paymentTestData)
            {
                var amount = ConvertAmountStringToLong(testData.TransactionInputData.Amount);

                TokenTransactionTest(testData.MerchantData.PublicKey, testData.MerchantData.SecretKey, amount, testData.TransactionValidationData);
            }

            if (paymentTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        [Test]
        public void ST_191()
        {
            var paymentTestData = _testData.Where(t => t.TestDataId.Equals("ST-191"));

            foreach (var testData in paymentTestData)
            {
                var amount = ConvertAmountStringToLong(testData.TransactionInputData.Amount);

                TokenTransactionTest(testData.MerchantData.PublicKey, testData.MerchantData.SecretKey, amount, testData.TransactionValidationData);
            }

            if (paymentTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// ST-14: Payment from Swipp 2.0 to Large Merchant (2C) across different DCs via QR-Code
        /// </summary>
        [Test]
        public void ST_192()
        {
            var paymentTestData = _testData.Where(t => t.TestDataId.Equals("ST-192"));

            foreach (var testData in paymentTestData)
            {
                var amount = ConvertAmountStringToLong(testData.TransactionInputData.Amount);

                TokenTransactionTest(testData.MerchantData.PublicKey, testData.MerchantData.SecretKey, amount, testData.TransactionValidationData);
            }

            if (paymentTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        private long ConvertAmountStringToLong(string amount)
        {
            if (amount.Contains(",") || amount.Contains("."))
            {
                amount = amount.Replace(",", "");
                amount = amount.Replace(".", "");
            }
            else
            {
                amount = amount + "00";
            }

            return Int64.Parse(amount);
        }

        private void TokenTransactionTest(string merchantPublic, string merchantPrivate, long amount, TransactionValidationData tvd)
        {
            //Assuming we are logged in and at mainscreen
            
            //Goto paymentview
            _appInteraction.MainViewInteraction.Payment();

            //Goto qr-code tab and get Qr code
            var paymentInteraction = _appInteraction.PaymentInteraction;
            paymentInteraction.SwitchTab();
            var qrToken = paymentInteraction.QrToken.Replace(" ", "");
            
            //Make a request based on the token... 
            var merchant = new Merchant(merchantPublic, merchantPrivate);
            var response = merchant.CreateRequestFromToken(qrToken, amount).Result;
            
            //Confirm payment
            paymentInteraction.ConfirmPayment();

            //Verify information at summaryview
            var summaryScreen = paymentInteraction.SummaryScreen;
            summaryScreen.Validate(tvd);

            paymentInteraction.SummaryScreen.CloseCompletedFlow();
        }

        /// <summary>
        /// payment to Organisation (1E) (Same DC)
        /// </summary>
        [Test]
        public void ST_187()
        {
            var numbersOfTimesRun = 0;
            var paymentTestData = _testData.Where(t => t.TestDataId.Equals("ST-187"));
            foreach (var testData in paymentTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    PerformPayment(testData, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    PerformPayment(testData);
                    numbersOfTimesRun++;
                }
            }

            if (paymentTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// payment to Organisation (1E) (Different DC)
        /// </summary>
        [Test]
        public void ST_188()
        {
            var numbersOfTimesRun = 0;
            var paymentTestData = _testData.Where(t => t.TestDataId.Equals("ST-188"));
            foreach (var testData in paymentTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    PerformPayment(testData, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    PerformPayment(testData);
                    numbersOfTimesRun++;
                }
            }

            if (paymentTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        [Test]
        public void ST_189()
        {
            var numbersOfTimesRun = 0;
            var paymentTestData = _testData.Where(t => t.TestDataId.Equals("ST-189"));
            foreach (var testData in paymentTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    PerformPayment(testData, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    PerformPayment(testData);
                    numbersOfTimesRun++;
                }
            }

            if (paymentTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        /// <summary>
        /// payment to Organisation (1E) (Different DC)
        /// </summary>
        [Test]
        public void ST_190()
        {
            var numbersOfTimesRun = 0;
            var paymentTestData = _testData.Where(t => t.TestDataId.Equals("ST-190"));
            foreach (var testData in paymentTestData)
            {
                //handles session Expired
                if (numbersOfTimesRun == 3)
                {
                    PerformPayment(testData, true);
                    numbersOfTimesRun = 0;
                }
                else
                {
                    PerformPayment(testData);
                    numbersOfTimesRun++;
                }
            }

            if (paymentTestData.Any()) return;
            if (TestDataSet.TestsWithoutDataShouldBeInconclusive)
            {
                Assert.Inconclusive("No testdata provided for test");
            }
            Assert.Fail("No testdata provided for test");
        }

        private void PerformPayment(TestDataSet testData, bool sessionExpired = false)
        {
            //Start transfer
            _appInteraction.MainViewInteraction.Payment();

            //Transfer to user (Choose recipient -> Enter amount -> Confirm -> Summary)
            var paymentInteraction = _appInteraction.PaymentInteraction;
            
            //Enter Recipient
            paymentInteraction.ChooseRecipientScreen.InputRecipient(testData.TransactionInputData.RecipientPhoneNumber);
            paymentInteraction.ChooseRecipientScreen.Validate(testData.TransactionValidationData);
            paymentInteraction.ChooseRecipientScreen.GoToNextInteraction();
            
            //Enter Amount
            paymentInteraction.EnterAmountScreen.EnterAmount(testData.TransactionInputData.Amount);
            paymentInteraction.EnterAmountScreen.Validate(testData.TransactionValidationData);
            paymentInteraction.EnterAmountScreen.GotoNextInteraction();

            //Enter Note and Confirm
            paymentInteraction.ConfirmationScreen.InputNoteToRecipient(testData.TransactionInputData.Note);
            paymentInteraction.ConfirmationScreen.Validate(testData.TransactionValidationData);
            paymentInteraction.ConfirmationScreen.Accept();

            //summary
            paymentInteraction.SummaryScreen.Validate(testData.TransactionValidationData);
            
            //Back to mainview
            paymentInteraction.SummaryScreen.CloseCompletedFlow();
        }

        [TearDown]
        public void TearDown()
        {
            SetupTeardown.TearDown();
        }
    }
}
